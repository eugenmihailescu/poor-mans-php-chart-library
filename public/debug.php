<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		aa2c6afe3c04671b46a496c2c3fb677053213dc7 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 14:25:59 2013 +0100 $
 * @file:		debug.php $
 * 
 * @id:	debug.php | Sat Jan 26 14:25:59 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'src/barcharth.php';
include_once 'src/piechart.php';
include_once 'src/linechart.php';
include_once 'src/stackedbarv100.php';
include_once 'src/stackedbarh100.php';

header("content-type:image/png");

define("ctVerticalBar", 0);
define("ctHorizontalBar", 1);
define("ctPie", 2);
define("ctScatteredPoints", 3);
define("ctLine", 4);
define("ctStackedBarV100", 5);
define("ctStackedBarH100", 6);

define("MAX_REC_COUNT", 5);
define("LOG_FILE", "/tmp/chart_data.txt");
define("LOG_ENABLED", true);
define("DEBUG", true);

$rand_names = array("Elvina", "Buchman", "Soila", "Cary", "Vernell", "Shameka", "Ellyn", "Milly", "Alfreda", "Clara ", "Gary",
		"Berna", "Juliana", "Hurlburt", "Sanderfur", "Jeramy", "Diane", "Hortense", "Edelen", "Swinton");

$chart_type = rand(ctVerticalBar, ctStackedBarH100);
$chart_type = ctStackedBarV100;

//////////////////////////////////////
// Step 1: create your chart object
////////////////////////////////////
switch ($chart_type) {
case ctVerticalBar:
	$chart = new BarChartV();
	break;
case ctHorizontalBar:
	$chart = new BarChartH();
	break;
case ctPie:
	$chart = new PieChart();
	break;
case ctScatteredPoints:
	$chart = new ScatterChart();
	break;
case ctLine:
	$chart = new LineChart();
	break;
case ctStackedBarV100:
	$chart = new StackedBarV100();
	break;
case ctStackedBarH100:
	$chart = new StackedBarH100();
	break;
}

if (LOG_ENABLED) {
	unlink(LOG_FILE);
	$fp = fopen(LOG_FILE, "a");
}

////////////////////////////////////
// Step 2: create/add some (random) data
// add data as a chart's serie
////////////////////////////////////
if (!DEBUG) {
	// first, make sure we have uniques keys for all series
	$keys = array();
	while (count($keys) < MAX_REC_COUNT) {
		$key = $rand_names[rand(0, count($rand_names) - 1)];
		if (!in_array($key, $keys))
			$keys[] = $key;
	}

	$value_kind = rand(-1, 1);//-1:only negative values;0:mixed values;+1:only positive values
	$minr = ($value_kind < 1 ? -1 : 1);
	$maxr = ($value_kind > -1 ? 1 : -1);
	for ($j = 0; $j < rand(1, 3); $j++) {
		$dataset = new XYDataSet();
		$factor = ($chart_type == ctScatteredPoints || $chart_type == ctLine) ? 20 : 1;
		for ($i = -rand(0, $factor > 1 ? MAX_REC_COUNT * $factor : 0); $i < MAX_REC_COUNT * $factor; $i++) {

			if ($factor == 1)
				$key = $keys[abs($i)];
			else
				$key = $i;

			if ($factor == 1)
				$value = rand(rand(30, 50) * $minr, rand(30, 50) * $maxr);
			else
				$value = ($value_kind * sqrt($i) + 1 - rand(-2, 2) * pow(2.718, -0.025 * $i)) * rand(-10, 10);

			if (!is_nan($value))
				$dataset->addItem(new XYValue($key, $value));
		}

		// add the dataset as a serie to be plotted on the chart
		$chart->addDataSerie($dataset, array(rand(0, 255), rand(0, 255), rand(0, 255)), "serie[$j]");

		if (LOG_ENABLED) {
			// write to log
			fwrite($fp, "// new dataset\n");
			fwrite($fp, "\$dataset = new XYDataSet();\n");
			for ($k = 0; $k < $dataset->getItemCount(); $k++)
				fwrite($fp,
						"\$dataset->addItem(new XYValue(\"" . $dataset->getItem($k)->getKey() . "\","
								. $dataset->getItem($k)->getValue() . "));\n");
			fwrite($fp, "\$chart->addDataSerie(\$dataset, array(rand(0, 255), rand(0, 255), rand(0, 255)), \"serie[$j]\");\n\n");
		}
	}

} else {
	$chart->getDataSeries()->loadFromCSV("data/points-numeric-key.csv", array(array(255, 0, 0), array(0, 0, 255)));
}

////////////////////////////////////
// Step 3: customize the chart settings
////////////////////////////////////
//$chart->setTitle("Sales vs Profit by Month");
$chart->setTooltipVisible(true);
$chart->setAxesNames("Value (K EURO)", "Month");
$chart->setSize(640, 480);
$chart->setMargins(50, 70, 70, 50);

if (LOG_ENABLED) {
	// close the log
	fclose($fp);
}

////////////////////////////////////
// Step 4: draw the chart
////////////////////////////////////
$chart->paint();

?>

