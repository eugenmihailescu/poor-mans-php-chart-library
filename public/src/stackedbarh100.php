<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		85a358ed12082c98953ec236974aafe23a850650 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 14:15:53 2013 +0100 $
 * @file:		stackedbarh100.php $
 * 
 * @id:	stackedbarh100.php | Sat Jan 26 14:15:53 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'stackedbarv100.php';

/**
 * Class that implements the (vertical|horizontal) stacked column chart functionalities.
 * 
 * @author Eugen Mihailescu
 */
class StackedBarH100 extends StackedBarV100 {

	function __construct() {
		parent::__construct();
		$this->setTitle("Horizontal 100% stacked column");
		$this->setReverseAxis(true);
	}
}
?>
