<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		04cb9cca6657bda4fba19b609c5b23b6be0b68ba $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 14:20:44 2013 +0100 $
 * @file:		barchartv.php $
 * 
 * @id:	barchartv.php | Sat Jan 26 14:20:44 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'basechart.php';

/**
 * the minimum space we reserve from the drawing width to use it as bar/series splitter
 */
define("RESERVED_SPACE", 50);

/**
 * the default width for one bar; the bar will autoshrink bellow this number if necessary 
 */
define("BAR_SIZE", 40);

/**
 * Class that implements the (vertical|horizontal) bar chart functionalities.
 * 
 * @author Eugen Mihailescu
 */
class BarChartV extends BaseChart {

	function __construct() {
		parent::__construct();
		$this->setTitle("Vertical bar chart");
	}

	/* (non-PHPdoc)
	 * @see BaseChart::DrawChart()
	 */
	protected function drawChart() {
		// define the bar default attributes
		$bar_count = $this->getDataSeries()->getRecordsCount();
		if ($bar_count == 0) {
			$this->invalidData();
			return;
		}

		$reverse = $this->getReverseAxis();// if reverse then draw a horizontal bar chart
		$white = $this->getColors()->getColor1(Color::clWhite());//color used to print the tooltips
		$grey = $this->getColors()->getColor1(Color::clDimGrey());//color used to outline the bars
		imagesetthickness($this->getCanvas(), 1);

		$reserve = RESERVED_SPACE;// we take this space as a reserve for bar_separator, just in case

		$hasTurnPoint = $this->hasTurnPoint1();//if has turn-point then we have negative values, null-point (aka origin), positive values

		$areaWidth = ($reverse ? $this->getDrawArea()->getHeight() : $this->getDrawArea()->getWidth()) - $reserve; //helps us to calc. how many bars we can draw
		$bar_width = ($areaWidth - $bar_count * BAR_SIZE) > 0 ? BAR_SIZE : floor($areaWidth / $bar_count);
		$bar_separator = ($areaWidth + $reserve - $bar_count * $bar_width) / ($bar_count); // calc the space between bars by available/unused space
		list($vScale, $offset) = $this->getScale();//scale the whole thing base on #keys and their values

		// init the drawing starting point
		if ($reverse)
			$head = $this->getDrawArea()->getTop() - $bar_separator / 2;
		else
			$head = $this->getMargins()->getLeft() + $bar_separator / 2;

		$keys = $this->getDataSeries()->getGroupedItems(GRP_KEY, false);//read the axis keys/values

		foreach ($keys as $key) {
			foreach ($this->getDataSeries()->getSeries() as $serie) {
				$value = $serie->getDataset()->getValueForKey($key);
				if ($value) {
					// calculate the bar height
					list($tw, $th) = $this->getTextRect($value);
					$tw1 = $reverse ? $tw : $th;
					$bar_height = ceil($value * $vScale);

					// calculate the coordinates for the drawn bar
					if ($reverse) {
						$x1 = $this->getMargins()->getLeft() - $offset * $vScale;
						$y1 = $head;
						$x2 = $x1 + $bar_height;
						$y2 = $y1 - $bar_width;
					} else {
						$x1 = $head;
						$y1 = $this->getDrawArea()->getTop() + $offset * $vScale - $bar_height;
						$x2 = $x1 + $bar_width;
						$y2 = $y1 + $bar_height;
					}
					$color = $this->getColors()->createColor1($serie->getColor());
					// fill the bar and draw its contour
					imagefilledrectangle($this->getCanvas(), $x1, $y1, $x2, $y2, $color);
					imagerectangle($this->getCanvas(), $x1, $y1, $x2, $y2, $grey);

					// draw the value just under the bar's top point
					if ($this->getTooltipVisible()) {
						$v = ($hasTurnPoint && $value < 0) ? -2 : (2 + ($reverse ? $tw : $th));
						if ($reverse)
							$this->printText($x2 - $v, $y2 + ($bar_width + $th) / 2, $value, false, 0, $white);
						else
							$this->printText($x1 + ($bar_width - $tw) / 2, $y1 + $v, $value, false, 0, $white);
					}

					$head = $reverse ? $y2 : $x2;
				}
			}
			$head += ($reverse ? -1 : 1) * $bar_separator;//step
		}
	}
}
?>
