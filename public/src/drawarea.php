<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c3d47cbf895f7c1bb72593b3ca2ed43ee5941c33 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:50:03 2013 +0100 $
 * @file:		drawarea.php $
 * 
 * @id:	drawarea.php | Sun Jan 20 18:50:03 2013 +0100 | Eugen Mihailescu  $
 * 
 */

/**
 * Class that provides storage for the rectanglar area's coordinates|dimensions.
 * This drawing area is supossed to contains two distinct rectangles, the second inside the first one:
 * - at the exterior is the canvas area
 * - at the interior is the drawing area
 * 
 * These names and what they represents are just conventions used when working with objects of this class. 
 * 
 * The virtual space between the exterior area (the canvas) and the interior area (the drawing) represents
 * the area margins. If the canvas and the drawing are the same then the margins are null.
 * 
 * NOTE: this class is used for both, the canvas and the drawing area so that some of this properties might
 * not be used together, although there are no restrictions.
 * 
 * @author Eugen Mihailescu
 *
 */
class DrawArea {

	private $bottom;
	private $height;
	private $left;
	private $right;
	private $top;
	private $width;

	function __construct() {
		$this->width = 0;
		$this->height = 0;
		$this->left = 0;
		$this->right = 0;
		$this->top = 0;
		$this->bottom = 0;
	}

	/**
	 * Return the distance from the canvas to the drawing area.
	 * @return the number of pixels
	 */
	function getBottom() {
		return $this->bottom;
	}

	/**
	 * Return the total height of the object of this class.
	 * @return the number of pixels
	 */
	function getHeight() {
		return $this->height;
	}

	/**
	 * Return the distance from the outer canvas to the inner drawing area left margin.
	 * @return the number of pixels
	 */
	function getLeft() {
		return $this->left;
	}

	/**
	 * Return the distance from the outer canvas to the inner drawing area right margin.
	 * @return the number of pixels
	 */
	function getRight() {
		return $this->right;
	}

	/**
	 * Return the dimensions|coordinates of this object.
	 * @return array with 6-items: width,height,top,bottom,left,right.
	 */
	function getRect() {
		return array($this->width, $this->height, $this->top, $this->bottom, $this->left, $this->right);
	}

	/**
	 * Return the distance from the outer canvas to the inner drawing area top margin.
	 * @return the number of pixels
	 */
	function getTop() {
		return $this->top;
	}

	/**
	 * Return the total width of the object of this class.
	 * @return the number of pixels
	 */
	function getWidth() {
		return $this->width;
	}

	/**
	 * Sets the coordinates (from the external canvas) of the drawing area.
	 * 
	 * @param int $top the distnace in pixels from the top outer canvas to the top inner drawing area
	 * @param int $bottom the distnace in pixels from the bottom outer canvas to the bottom inner drawing area
	 * @param int $left the distnace in pixels from the left outer canvas to the left inner drawing area
	 * @param int $right the distnace in pixels from the right outer canvas to the right inner drawing area
	 */
	function setMargins($top, $bottom, $left, $right) {
		$this->top = $top;
		$this->bottom = $bottom;
		$this->left = $left;
		$this->right = $right;
	}

	/**
	 * Sets the coordinates for this object (the canvas size and the distance to its inner drawing area)
	 * @param int $width the (outer) canvas width
	 * @param int $height the (outer) canvas height
	 * @param int $top the distnace in pixels from the top outer canvas to the top inner drawing area
	 * @param int $bottom the distnace in pixels from the bottom outer canvas to the bottom inner drawing area
	 * @param int $left the distnace in pixels from the left outer canvas to the left inner drawing area
	 * @param int $right the distnace in pixels from the right outer canvas to the right inner drawing area
	 */
	function setRect($width, $height, $top = 0, $bottom = 0, $left = 0, $right = 0) {
		$this->width = $width;
		$this->height = $height;
		$this->left = $left;
		$this->right = $right;
		$this->top = $top;
		$this->bottom = $bottom;
	}

}
