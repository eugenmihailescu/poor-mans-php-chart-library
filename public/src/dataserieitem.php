<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		64713c5c85c1dd0efc968c5dc01119d76778482f $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:49:20 2013 +0100 $
 * @file:		dataserieitem.php $
 * 
 * @id:	dataserieitem.php | Sun Jan 20 18:49:20 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'xydataset.php';

/**
 * Class that provides storage for one serie item
 * 
 * @author Eugen Mihailescu
 *
 */
class DataSerieItem {

	private $color;
	private $dataSet;
	private $name;

	function __construct($dataset, $color, $name) {
		$this->setColor($color);
		$this->setDataset($dataset);
		$this->setName($name);
	}

	/**
	 * Returns the dataset associated with this serie 
	 */
	function getDataset() {
		return $this->dataSet;
	}

	/**
	 * Returns the color associated with this serie 
	 */
	function getColor() {
		return $this->color;
	}

	/**
	 * Returns associated dataset key type
	 * @return int Can be one of TEXT_KEY,NUMERIC_KEY or DATE_KEY.
	 */
	function getDatasetKeyType() {
		return $this->dataSet->getKeyType();
	}

	/**
	 * Returns the name associated with this serie 
	 */
	function getName() {
		return $this->name;
	}

	/**
	 * Set the serie's associated color
	 * @param array $color an 3-item array that contains the values for r,g,b of color
	 */
	function setColor($color) {
		$this->color = $color;
	}

	/**
	 * Set the serie's associated XYDataset
	 * @param XYDataset $dataset
	 */
	function setDataset($dataset) {
		$this->dataSet = $dataset;
	}

	/**
	 * Set the serie's associated name
	 * @param string $name
	 */
	function setName($name) {
		$this->name = $name;
	}

}
