<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		f6d36715f1581aaed8e563b9dc94fa54510eed20 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 15:51:02 2013 +0100 $
 * @file:		stackedbarv100.php $
 * 
 * @id:	stackedbarv100.php | Sat Jan 26 15:51:02 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'basechart.php';

/**
 * Class that implements the (vertical|horizontal) stacked column chart functionalities.
 * 
 * @author Eugen Mihailescu
 */
class StackedBarV100 extends BaseChart {

	function __construct() {
		parent::__construct();
		$this->setTitle("Vertical 100% stacked column");
	}

	/*
	 * (non-PHPdoc)
	 * @see BaseChart::drawAxis()
	 */
	protected function drawAxis($axeKind) {
		$reverse = $this->getReverseAxis();
		if (($reverse && $axeKind == Y_AXIS) || (!$reverse && $axeKind == X_AXIS)) {
			parent::drawAxis($axeKind);
			return;
		}

		$from = $reverse ? $this->getMargins()->getLeft() : $this->getMargins()->getTop();
		$to = $from + ($reverse ? $this->getDrawArea()->getWidth() : $this->getDrawArea()->getHeight());
		$k = $reverse ? $this->getMargins()->getTop() + $this->getDrawArea()->getHeight() : $this->getMargins()->getLeft();
		$step = 10;
		$v = 0;
		for ($i = $to; $i >= $from; $i -= ($to - $from) / $step) {
			if ($reverse) {
				imageline($this->getCanvas(), $i, $k, $i, $k + 4, $this->getColors()->getColor1(Color::clGrey()));
				$t = 100 - $v;
			} else {
				imageline($this->getCanvas(), $k, $i, $k - 4, $i, $this->getColors()->getColor1(Color::clGrey()));
				$t = $v;
			}

			list($tw, $th) = $this->getTextRect($t);
			if ($reverse)
				$this->printText($i - $tw / 2, $k + 10 + $th, $t);
			else
				$this->printText($k - 6 - $tw, $i + $th / 2, $t);

			$v += $step;
		}

	}

	/* (non-PHPdoc)
	 * @see BaseChart::DrawChart()
	 */
	protected function drawChart() {
		// define the bar default attributes
		$bar_count = $this->getDataSeries()->getRecordsCount() / $this->getDataSeries()->getCount();
		if ($bar_count == 0) {
			$this->invalidData();
			return;
		}

		$reverse = $this->getReverseAxis();// if reverse then draw a horizontal bar chart
		$white = $this->getColors()->getColor1(Color::clWhite());//color used to print the tooltips
		$grey = $this->getColors()->getColor1(Color::clDimGrey());//color used to outline the bars
		imagesetthickness($this->getCanvas(), 1);

		$reserve = RESERVED_SPACE;// we take this space as a reserve for bar_separator, just in case

		$areaWidth = ($reverse ? $this->getDrawArea()->getHeight() : $this->getDrawArea()->getWidth()) - $reserve; //helps us to calc. how many bars we can draw
		$areaHeight = ($reverse ? $this->getDrawArea()->getWidth() : $this->getDrawArea()->getHeight()); //helps us to calc. how many bars we can draw
		$bar_width = ($areaWidth - $bar_count * BAR_SIZE) > 0 ? BAR_SIZE : floor($areaWidth / $bar_count);
		$bar_separator = ($areaWidth + $reserve - $bar_count * $bar_width) / ($bar_count); // calc the space between bars by available/unused space

		// init the drawing starting point
		if ($reverse)
			$head = $this->getDrawArea()->getTop() - $bar_separator / 2;
		else
			$head = $this->getMargins()->getLeft() + $bar_separator / 2;

		$keys = $this->getDataSeries()->getGroupedItems(GRP_KEY, false);//read the axis keys/values
		foreach ($keys as $key) {
			$totalValue = $this->getDataSeries()->getTotalValueByKey($key,false);
			if ($totalValue <= 0) {
				$this->invalidData();
				return;
			}
			$lValue = 0;
			foreach ($this->getDataSeries()->getSeries() as $serie) {
				$value = $serie->getDataset()->getValueForKey($key) / $totalValue;
				if ($value>0 && $value <= 1) {
					// calculate the coordinates for the drawn bar
					if ($reverse) {
						$x1 = $this->getMargins()->getLeft() + $lValue;
						$y1 = $head;
						$x2 = $x1 + $areaHeight * $value;
						$y2 = $y1 - $bar_width;
					} else {
						$x1 = $head;
						$y1 = $this->getMargins()->getTop() + $lValue;
						$x2 = $x1 + $bar_width;
						$y2 = $y1 + $areaHeight * $value;
					}
					$color = $this->getColors()->createColor1($serie->getColor());

					// fill the bar and draw its contour
					imagefilledrectangle($this->getCanvas(), $x1, $y1, $x2, $y2, $color);
					imagerectangle($this->getCanvas(), $x1, $y1, $x2, $y2, $grey);

					// draw the value just under the bar's top point
					if ($this->getTooltipVisible()) {
						// calculate the bar height
						$proc = round(100 * $value, 1) . "%";
						list($tw, $th) = $this->getTextRect($proc);

						if ($reverse)
							$this->printText(($x1 + $x2 - $tw) / 2, $y2 + ($bar_width + $th) / 2, $proc, false, 0, $white);
						else
							$this->printText($x1 + ($bar_width - $tw) / 2, ($y1 + $y2 + $th) / 2, $proc, false, 0, $white);
					}
					$lValue += $areaHeight * $value;
				}
			}
			$head += ($reverse ? -1 : 1) * ($bar_separator + $bar_width);//step
		}
	}
}
?>
