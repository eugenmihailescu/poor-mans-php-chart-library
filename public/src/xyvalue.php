<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		64713c5c85c1dd0efc968c5dc01119d76778482f $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:49:20 2013 +0100 $
 * @file:		xyvalue.php $
 * 
 * @id:	xyvalue.php | Sun Jan 20 18:49:20 2013 +0100 | Eugen Mihailescu  $
 * 
 */

/**
 * Class that provides the records/items for a XYDataSet dataset
 * 
 * @author Eugen Mihailescu
 *
 */
class XYValue {

	private $color;
	private $key;
	private $value;

	function __construct($key, $value, $color = null) {
		$this->key = $key;
		$this->value = $value;
		$this->color = $color;
	}

	/**
	 * Returns the item's color
	 * @return array<r,g,b>
	 */
	function getColor() {
		return $this->color;
	}

	/**
	 * Returns the item's key
	 * @return str
	 */
	function getKey() {
		return $this->key;
	}

	/**
	 * Returns the item's value
	 * @return number
	 */
	function getValue() {
		return $this->value;
	}

	/**
	 * Set the color for the item
	 * @param array<r,g,b> $color
	 */
	function setColor($color) {
		$this->color = $color;
	}
}
?>
