<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c3d47cbf895f7c1bb72593b3ca2ed43ee5941c33 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:50:03 2013 +0100 $
 * @file:		fontdef.php $
 * 
 * @id:	fontdef.php | Sun Jan 20 18:50:03 2013 +0100 | Eugen Mihailescu  $
 * 
 */

/**
 * Class that provides storage for a font definition.
 * 
 * On this version it's poorly used, if any.
 * 
 * @author Eugen Mihailescu
 *
 */
class FontDefinition {

	private $bold;
	private $name;
	private $regular;
	private $size;

	function __construct($name, $regular_path, $bold_path, $size) {
		$this->setFont($name, $regular_path, $bold_path, $size);
	}

	/**
	 * Returns true if font is bold, false otherwise (which means regular).
	 * @return boolean
	 */
	function getFontBold() {
		return $this->bold;
	}

	/**
	 * Returns true if font is regular, false otherwise (which means bold).
	 * @return boolean
	 */
	function getFontRegular() {
		return $this->regular;
	}

	/**
	 * Returns the font size in pixels.
	 * @return int
	 */
	function getFontSize() {
		return $this->size;
	}

	/**
	 * Set the properties of this font object.
	 *  
	 * @param string $name the name of the font (used only internaly)
	 * @param string $regular_path the relative path (on server) of the file for the regular font.
	 * @param string $bold_path the relative path (on server) of the file for the bold font.
	 * @param int $size the size in pixels for this font definition.
	 */
	function setFont($name, $regular_path, $bold_path, $size) {
		$this->regular = $regular_path;
		$this->bold = $bold_path;
		$this->name = $name;
		$this->size = $size;
	}

}
?>
