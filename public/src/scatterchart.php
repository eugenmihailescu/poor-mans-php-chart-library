<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		7396f653ec72af99c1d0f2353387d23319babb5e $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 09:29:18 2013 +0100 $
 * @file:		scatterchart.php $
 * 
 * @id:	scatterchart.php | Sat Jan 26 09:29:18 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'basechart.php';
include_once 'lineareg.php';

/**
 * The max diameter of the point plotted on chart 
 */
define("MAX_POINT_SIZE", 7);

/**
 * The generic name (on legend) of the regression line. 
 */
define("BESTFIT_LEGEND", "TREND");

/**
 * Class that provides the functionalities for scattered points and line chart.
 * 
 * @author Eugen Mihailescu
 *
 */
class ScatterChart extends BaseChart {

	private $connectedPoints;
	private $sorted;
	private $trend_visible;
	private $scale;

	function __construct() {
		parent::__construct();

		// by default we don't want to have a sorted dataset but it should be sorted in the case of a LineChart, though
		$this->setSorted(false);

		$this->setTitle("Scatter chart");
		$this->setConnectedPoints(false);//by default the scattered points are not supposed to be connected with each other
		$this->setTrendVisible(true);//by default we don't want the trend line to be visible
		$this->setGridVisible(false);
	}

	/**
	 * Configure the chart to connect|not connect the points between them with a line.
	 * @param bool $connected if true the points will be connected with eachother by lines.
	 */
	function setConnectedPoints($connected) {
		$this->connectedPoints = $connected;
	}

	/**
	 * Configure the chart to sort its items before plotting them.
	 * @param bool $sorted if true then the items will be sorted ahead (usefull especially for line chart)
	 */
	function setSorted($sorted) {
		$this->sorted = $sorted;
	}

	function setTrendVisible($visible) {
		$this->trend_visible = $visible;
	}

	/**
	 * Plot the points given by the serie's dataset.
	 * 
	 * @param XYDataset $dataset the dataset whose keys|values are plotted
	 * @param RGB $color the r,g,b pair of values that represent the color used to plot the points
	 * @param string $name the name of the serie as it would appear on legend
	 * @param float $scale the scale to be used when plotting the points on the graph
	 */
	private function plotPoints($dataset, $color, $name) {
		imagesetthickness($this->getCanvas(), 0);//to plot a smooth circle

		// precalculate some heavy used variables
		$points = $dataset->getItemCount();
		$left = $this->getMargins()->getLeft();
		$top = $this->getCanvasArea()->getHeight() - $this->getMargins()->getBottom();

		list($min_Xvalue, $max_Xvalue, $xScale, $xOffset, $min_Yvalue, $max_Yvalue, $yScale, $yOffset) = $this->scale;

		// the point's size is scaled down automatically
		$point_size = min(MAX_POINT_SIZE, $points == 0 ? MAX_POINT_SIZE : $this->getDrawArea()->getWidth() / $points);

		$rlcolor = $this->getColors()->getComplementaryColor2($color);//used by the regression line
		$color = $this->getColors()->createColor1($color);//used by the scattered points

		$key_type = $dataset->getKeyType();//auto-detect the data type of the key

		$lx_ = null;
		$ly_ = null;

		$fp_ = false;//first point plotted
		$fp = false;//first point on regression line
		$fx = null;
		$fy = null;

		// scatter the (x,y) points
		for ($i = 0; $i < $points; $i++) {

			$x_value = $dataset->getItem($i)->getKey();
			$y_value = $dataset->getItem($i)->getValue();

			///////////////////////////////
			// scatter the point
			$x = $this->getDataSeries()->getKeyDifference($x_value, $min_Xvalue) * $xScale;
			$y = ($y_value - $min_Yvalue - $yOffset) * $yScale;

			//TODO : antialiased circle
			if ($x >= 0 && $x <= $this->getDrawArea()->getWidth() && $y >= 0 && $y <= $this->getDrawArea()->getHeight()) {

				imagefilledarc($this->getCanvas(), $left + $x, $top - $y, $point_size, $point_size, 0, 359, $color,
						IMG_ARC_NOFILL);

				if ($this->getTooltipVisible())
					$this->printText($left + $x + 5, $top - $y, $y_value);

				//interconnect the points
				if ($this->connectedPoints && $fp_
						&& ($lx_ >= 0 && $lx_ <= $this->getDrawArea()->getWidth() && $ly_ <= $top && $ly_ >= 0)) {
					$offset_x = $point_size * abs($x - $lx_) / sqrt(pow($x - $lx_, 2) + pow($y - $ly_, 2)) / 2;
					$offset_y = $point_size * abs($y - $ly_) / sqrt(pow($x - $lx_, 2) + pow($y - $ly_, 2)) / 2;

					imageline($this->getCanvas(), $left + ($lx_ + $offset_x), $top - ($ly_ + $offset_y),
							$left + ($x - $offset_x), $top - ($y - $offset_y), $color);
				}
			}

			$fp_ = true;

			$lx_ = $x;
			$ly_ = $y;
		}
	}

	/**
	 * Calculates and draw a regression line for the specified scattered points.
	 * @param array $data an array of (key,value) points for which should calculate|plot the regression line
	 * @param float $scale the scale to be used when plotting the regression line on the chart
	 * @param RGB $color the color to be used when drawing the regression line on the chart
	 */
	private function plotRegressionLine($data, $key_type, $color) {
		list($min_Xvalue, $max_Xvalue, $xScale, $xOffset, $min_Yvalue, $max_Yvalue, $yScale, $yOffset) = $this->scale;
		$left = $this->getMargins()->getLeft();
		$top = $this->getCanvasArea()->getHeight() - $this->getMargins()->getBottom();

		$fp = false;//first point on regression line
		$fx = null;
		$fy = null;
		$x_value = null;

		// calculate the linear regression for the (x,y) scattered data
		$linreg = new LinearRegression($data);
		list($m, $b) = $linreg->getGetEquation();//get the linear regression equation (m=slope,b=y-intercept)

		foreach ($data as $point) {
			list($x_value) = $point;

			$y = $m * $x_value + $b;

			if ($key_type == DATE_KEY)
				$diff_x = $this->getDataSeries()->getKeyDifference($x_value, $min_Xvalue);
			else
				$diff_x = $x_value - $min_Xvalue;

			$scaled_x = $diff_x * $xScale;
			$scaled_y = ($y - $min_Yvalue - $yOffset) * $yScale;

			if (!$fp
					|| ($fx <= 0 || $fx >= $this->getDrawArea()->getWidth() || $fy <= 0
							|| $fy >= $this->getDrawArea()->getHeight())) {
				$fx = $scaled_x;
				$fy = $scaled_y;
			}

			$fp = true;
			if ($scaled_x > 0 && $scaled_x < $this->getDrawArea()->getWidth())
				$lx = $scaled_x;
			if ($scaled_y > 0 && $scaled_y < $this->getDrawArea()->getHeight())
				$ly = $scaled_y;
		}
		if (empty($lx) || empty($ly))
			return;

		imageline($this->getCanvas(), $left + $lx, $top - $ly, $left + $fx, $top - $fy, $color);
		imageline($this->getCanvas(), $left + $lx, $top - $ly - 1, $left + $fx, $top - $fy - 1, $color);
	}

	/**
	 * (non-PHPdoc)
	 * @see BaseChart::DrawChart()
	 */
	protected function drawChart() {
		$regdata = array();
		$seriecolors = array();

		list($min_Xvalue, $max_Xvalue) = $this->getDataSeries()->getMinMaxKey();
		list($min_Yvalue, $max_Yvalue) = $this->getDataSeries()->getMinMaxValue();

		list($xScale, $xOffset) = $this->getScale(1, $min_Xvalue);//calc the X scale
		list($yScale, $yOffset) = $this->getScale(2, $min_Yvalue);//calc the Y scale

		$this->scale = array($min_Xvalue, $max_Xvalue, $xScale, $xOffset, $min_Yvalue, $max_Yvalue, $yScale, $yOffset);
		$dataSeries = $this->getDataSeries()->getSeries();
		if (empty($dataSeries)) {
			$this->invalidData();
			return;
		}

		foreach ($dataSeries as $serie) {
			$dataset = $serie->getDataset();
			if ($this->sorted)
				$dataset = $dataset->getSortedData();

			$color = $serie->getColor();
			$name = $serie->getName();

			$this->plotPoints($dataset, $color, $name);
			$regdata = array_merge($regdata, $dataset->getXYArray());
			$seriecolors[] = $serie->getColor();
		}

		if (!$this->trend_visible || $this->getDataSeries()->getCount() < 2)
			return;

		$rcolors = $this->getColors()->randomColors(1, $seriecolors);
		$color = $this->getColors()->createColor1($rcolors[0]);

		$this->plotRegressionLine($regdata, $this->getDataSeries()->getSerie(0)->getDatasetKeyType(), $color);
		$rect = $this->getTextRect($this->getDataSeries()->getLargestSerieName());
		imagesetthickness($this->getCanvas(), 0);
		$this->printLegendItem($rect, BESTFIT_LEGEND, $color, $this->getDataSeries()->getCount());
	}

	/**
	 * (non-PHPdoc)
	 * @see BaseChart::init()
	 */
	protected function init() {
		list($min_Yvalue, $max_Yvalue) = $this->getDataSeries()->getMinMaxValue();
		$this->setNullPoint($min_Yvalue);
	}
}
?>
