<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		5b899cdf785ad96c85534b523e9def2660e4bcee $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:47:00 2013 +0100 $
 * @file:		colors.php $
 * 
 * @id:	colors.php | Sun Jan 20 18:47:00 2013 +0100 | Eugen Mihailescu  $
 * 
 */

/**
 * Class that provides color management
 * 
 * @author Eugen Mihailescu
 *
 */
class Color {

	private $colorList;
	private $im;

	function __construct($im) {
		$this->im = $im;
		$this->colorList = array();
	}

	/**
	 * Create/reuse a color for the $im image resource by the RGB value
	 * @param int $r
	 * @param int $g
	 * @param int $b
	 * @return the index of the color within the resource image's palette
	 */
	function createColor($r, $g, $b) {
		$numColors = imagecolorstotal($this->im);//get the # of color used
		$color = $this->getColor($r, $g, $b, ($numColors >= 255));//if exceed the palette, get the most closest possible otherwise we search only for the exact color
		// if not found either way, create it
		if ($color == -1) {
			imagecolorallocate($this->im, $r, $g, $b);
			$color = imagecolorallocatealpha($this->im, $r, $g, $b, 20);
			$this->colorList[$color] = sprintf("%d,%d,%d", $r, $g, $b);
		}
		return $color;
	}

	/**
	 * Creates a new color based on the specified RGB values
	 *
	 * @param array $RGB a 3-item array which contains the r,g,b values of the color
	 * @return the index of the color within the resource image's palette
	 */
	function createColor1($RGB) {
		list($r, $g, $b) = $RGB;
		return $this->createColor($r, $g, $b);
	}

	/**
	 * Lookup for the color specified by RGB values in the resource image's pallete and returns its corresponding index.
	 * @param int $r
	 * @param int $g
	 * @param int $b
	 * @return the index of the color withing the resource image's palette or -1 if not found.
	 */
	function getColor($r, $g, $b, $closest_possible = false) {
		$color = array_search(sprintf("%d,%d,%d", $r, $g, $b), $this->colorList);
		if ($color > 0)
			$result = $color;
		else
			$result = $closest_possible ? imagecolorclosest($this->im, $r, $g, $b) : -1;
		return $result;
	}

	/**
	 * Return the color resource Id for the specified RGB array
	 *
	 * @param array $RGB an 3-item array that provides the r,g,b values for the color
	 * @return the resource Id of the color
	 */
	function getColor1($RGB) {
		list($r, $g, $b) = $RGB;
		return $this->getColor($r, $g, $b);
	}

	/**
	 * Calulates the distance between 2 colors
	 *
	 * A color is represented (in a 3D space) by its R,G,B value.
	 * To find the similarity between 2 colors we have to find the
	 * distance between them, i.e. how far is the point P(r1,g1,b1) from the point Q(r2,g2,b2)
	 *
	 * In a n-space you measure the distance between 2 points using the Euclidean distance:
	 * http://en.wikipedia.org/wiki/Euclidean_distance
	 * http://en.wikipedia.org/wiki/Color_difference
	 *
	 * d(P,Q)=d(Q,P)=sqrt[(r1-r2)^2+(g1-g2)^2+(b1-b2)^2]
	 *
	 * @param RGB color array $color1
	 * @param RGB color array $color2
	 */
	function getColorSimilarities($color1, $color2) {
		list($r1, $g1, $b1) = $color1;
		list($r2, $g2, $b2) = $color2;
		return sqrt(pow(30 * ($r1 - $r2), 2) + pow(59 * ($g1 - $g2), 2) + pow(11 * ($b1 - $b2), 2));
	}

	/**
	 * Calculate the complementary color for the specified parameter, choosen
	 * from the oposite side of the color wheel
	 *
	 * @param color
	 *            the color
	 * @return
	 */
	function getComplementaryColor($r, $g, $b) {
		$HSL = $this->getRGB2HSL($r, $g, $b);
		$HSL[0] += 0.5;
		if ($HSL[0] > 1)
			$HSL[0] -= -1;

		$RGB = $this->getHSL2RGB($HSL);
		return $this->createColor($RGB[0], $RGB[1], $RGB[2]);
	}

	/**
	 * Return the complementary color for the specified RGB
	 *
	 * @param array $RGB a 3-item array providing the r,g,b values of the color
	 * @return int the Id of the resource color created
	 */
	function getComplementaryColor2($RGB) {
		list($r, $g, $b) = $RGB;
		return $this->getComplementaryColor($r, $g, $b);
	}

	/**
	 * Generate n-random colors
	 *
	 * @param int $size the number of colors to generate
	 * @param array $except_list an array containing elements in the form of 3-item array that represents the r,g,b values of the color that shouldn't be created
	 * @return array that contains the newly created colors (each color as a 3-item array of r,g,b values)
	 */
	function randomColors($size, $except_list) {
		$result = array();
		$i = 0;
		$t = 0;
		while ($t < $size) {
			$c = $this->randomColor();
			if (!(in_array($c, $except_list) && in_array($c, $result))) {
				$min = 200;
				// make sure the found color doesn't look like $result
				for ($j = 0; $j < $t; $j++)
					$min = min($min, $this->getColorSimilarities($result[$j], $c));
				// make sure the found color doesn't look like $except_list
				for ($j = 0; $j < count($except_list); $j++)
					$min = max($min, $this->getColorSimilarities($except_list[$j], $c));
				// we assume that distance of 200 is a good delta between (color1, color2)
				if ($t == 0 || $min > 200)
					$result[] = $c;
			}
			$t = count($result);
		}
		return $result;
	}

	/**
	 * Set the canvas that owns the color resources
	 * @param unknown $im
	 */
	function setCanvas($im) {
		$this->im = $im;
	}

	/**
	 * Convert a HSL values to RGB values
	 *
	 * @param HSL
	 *            the array containing the H,S,L values
	 * @return an int array containing R,G,B values
	 */
	private function getHSL2RGB($HSL) {
		list($H, $S, $L) = $HSL;
		if ($S == 0) {
			$R = $L * 255;
			$G = $L * 255;
			$B = $L * 255;
		} else {
			if ($L < 0.5)
				$var_2 = $L * (1 + $S);
			else
				$var_2 = ($L + $S) - ($S * $L);

			$var_1 = 2 * $L - $var_2;

			$R = 255 * $this->getHue2RGB($var_1, $var_2, $H + (1.0 / 3.0));
			$G = 255 * $this->getHue2RGB($var_1, $var_2, $H);
			$B = 255 * $this->getHue2RGB($var_1, $var_2, $H - (1.0 / 3.0));
		}
		return array(round($R, 0), round($G, 0), round($B, 0));
	}

	/**
	 * Convert a Hue specter to RGB value
	 *
	 * @param v1
	 * @param v2
	 * @param vH
	 * @return the RGB value based on the Hue specter
	 */
	private function getHue2RGB($v1, $v2, $vH) {
		if ($vH < 0)
			$vH += 1;
		if ($vH > 1)
			$vH -= 1;
		if ((6 * $vH) < 1)
			return ($v1 + ($v2 - $v1) * 6 * $vH);
		if ((2 * $vH) < 1)
			return ($v2);
		if ((3 * $vH) < 2)
			return ($v1 + ($v2 - $v1) * ((2.0 / 3.0) - $vH) * 6.0);
		return $v1;
	}

	/**
	 * Return the HSL values for the specified color.
	 *
	 * @param color
	 *            the color for which calculates the HSB values
	 * @return an array of 3 double whose values are between [0..1]
	 */
	private function getRGB2HSL($r, $g, $b) {
		$_R = 1.0 * $r / 255.0;
		$_G = 1.0 * $g / 255.0;
		$_B = 1.0 * $b / 255.0;

		$_Min = min($_R, min($_G, $_B));
		$_Max = max($_R, max($_G, $_B));
		$_dMax = $_Max - $_Min;

		$H = 0;
		$S = 0;
		$L = ($_Max + $_Min) / 2.0;

		if ($_dMax == 0) {
			$H = 0;
			$S = 0;
		} else {
			if ($L < 0.5)
				$S = $_dMax / ($_Max + $_Min);
			else
				$S = $_dMax / (2.0 - $_Max - $_Min);

			$_dR = ((($_Max - $_R) / 6.0) + ($_dMax / 2.0)) / $_dMax;
			$_dG = ((($_Max - $_G) / 6.0) + ($_dMax / 2.0)) / $_dMax;
			$_dB = ((($_Max - $_B) / 6.0) + ($_dMax / 2.0)) / $_dMax;

			if ($_R == $_Max)
				$H = $_dB - $_dG;
			else if ($_G == $_Max)
				$H = (1.0 / 3.0) + $_dR - $_dB;
			else if ($_B == $_Max)
				$H = (2.0 / 3.0) + $_dG - $_dR;

			if ($H < 0)
				$H += 1;
			if ($H > 1)
				$H -= 1;
		}
		return array($H, $S, $L);
	}

	/**
	 * Return a random color
	 *
	 * @return an 3-item array which contains the r,g,b values of the random color
	 */
	private function randomColor() {
		return array(rand(0, 255), rand(0, 255), rand(0, 255));
	}

	/**
	 * Returns the r,g,b values for the dim grey color
	 * It's mostly used for fg color and rect. contour
	 * @return 3-item array with the color's RGB value
	 */
	static function clDimGrey() {
		return array(0x69, 0x69, 0x69);
	}

	/**
	 * Returns the r,g,b values for the grey color
	 * It's mostly used for axes color
	 * @return 3-item array with the color's RGB value 
	 */
	static function clGrey() {
		return array(0x80, 0x80, 0x80);
	}
	
	/**
	 * Returns the r,g,b values for the navy color
	 * It's mostly used for title and axes name
	 * @return 3-item array with the color's RGB value
	 */
	static function clDarkSlateGrey() {
		return array(0x2F, 0x4F, 0x4F);
	}

		
	/**
	 * Returns the r,g,b values for the silver color
	 * It's mostly used for grid/drawing area bg color
	 * @return 3-item array with the color's RGB value 
	 */
	static function clSilver() {
		return array(0xDC, 0xDC, 0xDC);
	}

	/**
	 * Returns the r,g,b values for the white color
	 * It's mostly used for printing the tooltip values
	 * @return 3-item array with the color's RGB value 
	 */
	static function clWhite() {
		return array(0xFF, 0xFF, 0xFF);
	}

}
?>
