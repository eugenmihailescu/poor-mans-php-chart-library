<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c3d47cbf895f7c1bb72593b3ca2ed43ee5941c33 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:50:03 2013 +0100 $
 * @file:		fonts.php $
 * 
 * @id:	fonts.php | Sun Jan 20 18:50:03 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'fontdef.php';

/**
 * Class that provides the container for one or more font definitions.
 * It was aimed to provide a global storage for all fonts (and their definitions)
 * that  are used within this charting library.
 * 
 * On this version it's poorly used, if any.
 * 
 * @author Eugen Mihailescu
 *
 */
class FontDefinitions {

	private $items;

	/**
	 * Add a new font definition to this font container.
	 * 
	 * @param string $name the name of the font (used only internaly)
	 * @param string $regular_path the relative path (on server) of the file for the regular font.
	 * @param string $bold_path $bold_path the relative path (on server) of the file for the bold font.
	 * @param int $size the size in pixels for this font definition.
	 */
	function addFontDefinition($name, $regular_path, $bold_path, $size) {
		if ($this->fontNameExists($name))
			die("Font with this name \"$name\" already exists.");
		$this->items[] = new FontDefinition($name, $regular_path, $bold_path, $size);
	}

	/**
	 * Add a new font definition to this font container.
	 * @param FontDefinition $fontDef
	 */
	function addFontDefinition1($fontDef) {
		$this->items[] = $fontDef;
	}

	/**
	 * Removes the font, identified by its name, from container.
	 * @param string $name the name of the font to remove
	 */
	function delFontDefinition($name) {
		$found = $this->getFontByName($name);
		unset($found);
	}

	/**
	 * Removes the font, identified by its index, from container.
	 * @param int $index the index of the font in the container that has to be removed.
	 */
	function delFontDefinition1($index) {
		unset($this->items[$index]);
	}

	/**
	 * Verifies the existence of the specified font in the container.
	 * @param string $name the font name to search for.
	 * @return boolean returns true if found, false otherwise.
	 */
	function fontNameExists($name) {
		$fontDef=$this->getFontByName($name);
		return !empty($fontDef);
	}

	/**
	 * Returns the font definition from the container, identified by its 0-index. 
	 * @param int $index the index of the font.
	 * @return a FontDefinition object or null if no font found.
	 */
	function getFontByIndex($index) {
		return $this->items[$index];
	}

	/**
	 * Returns the font definition from the container, identified by its name.
	 * @param string $name the name of the font that has to be identified.
	 * @return a FontDefinition object or null if no font found.
	 */
	function getFontByName($name) {
		$found = null;
		foreach ($items as $fontDef)
			if (strcm($name, $fontDef->getName()) == 0) {
				$found = $fontDef;
				break;
			}
		return $found;
	}

	/**
	 * Returns the number of font definitions within the container.
	 * @return number
	 */
	function getFontDefCount() {
		return count($this->items);
	}

}
