<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		94920cd54687c803efe5969932ae956c9f4b5d5e $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 15:50:21 2013 +0100 $
 * @file:		basechart.php $
 * 
 * @id:	basechart.php | Sat Jan 26 15:50:21 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'xydataset.php';
include_once 'colors.php';
include_once 'fonts.php';
include_once 'drawarea.php';
include_once 'dataseries.php';

/**
 * set to true if your PHP server is compiled with GD antialias, false otherwise 
 */
define("PHP_COMPILED_WITH_GD", true);

/**
 * the distance between grid lines
 */
define("GRID_STEP", 15);

/**
 * constants used to specify what it's shown on axes 
 */
define("GRP_VALUE", 0); // values are shown
define("GRP_KEY", 1); // text keys are shown

/**
 * constants used to identify the axis
 */
define("X_AXIS", 0);// represents the horizontal X-axis
define("Y_AXIS", 1);// represents the vertical Y-axis

/**
 * The current version of the library, automatically incremented on git commit
 */
define("LIB_VERSION", "0.1a-32-gf6d3671");

/**
 * The base class that provides the main functionalities for drawing a chart
 * 
 * @author Eugen Mihailescu
 */
class BaseChart {

	private $antialiased;
	private $axes_fg_color;
	private $bg_color;
	private $canvas;
	private $canvasArea;
	private $colors;
	private $currentFont;
	private $dataSeries;
	private $drawArea;
	private $drawArea_bg_color;
	private $fg_color;
	private $fontList;
	private $grid_visible;
	private $legend_visible;
	private $marginArea;
	private $null_point;
	private $reverse_axis;
	private $title;
	private $title_fg_color;
	private $tooltip_visible;
	private $x_axis_name;
	private $x_axis_visible;
	private $y_axis_name;
	private $y_axis_visible;

	function __construct() {
		date_default_timezone_set('UTC');// this class relay on UTC timezone; you might change it as you pleased.

		$this->setFgColor(Color::clDimGrey());
		$this->setBgColor(Color::clSilver());
		$this->setDrawAreaBgColor(Color::clWhite());

		$this->drawArea = new DrawArea();
		$this->canvasArea = new DrawArea();
		$this->marginArea = new DrawArea();
		$this->dataSeries = new DataSeries();
		$this->colors = null;
		$this->canvas = -1;
		$this->setGridVisible(true);
		$this->reverse_axis = false;
		$this->setAxisVisible(true, true);
		$this->setAxesNames("X-axis", "Y-axix", Color::clDarkSlateGrey());
		$this->setLegendVisible(true);
		$this->setTooltipVisible(false);
		$this->setTitle(get_class(), Color::clDarkSlateGrey());
		$this->setMargins(75, 75, 75, 75);
		$this->antialiased = true;
		$this->setSize(500, 250);
		$this->fontList = new FontDefinitions();
		$this->currentFont = new FontDefinition("arial", "./fonts/arial.ttf", "./fonts/arialbd.ttf", 8);
		$this->fontList->addFontDefinition1($this->currentFont);
		$this->setNullPoint(0);
	}

	function __destruct() {
		imagedestroy($this->canvas);
	}

	/**
	 * Add a new data serie to chart with its specified color
	 * @param XYdataset $dataset : the dataset that represents the serie
	 * @param color $color : an array of RGB values that represents the color used for the serie
	 * @param string $name : the name of the serie
	 */
	function addDataSerie($dataset, $color, $name) {
		$this->dataSeries->addSerie($dataset, $color, $name);
		$this->fixMargins();
	}

	/**
	 * Add a new data serie to chart with a computer chosen random color
	 * @param XYdataset $dataset : the dataset that represents the serie
	 * @param string $name : the name of the serie
	 */
	function addDataSerie1($dataset, $name) {
		$color = $this->colors->randomColors($dataset->getItemCount(), array($this->bg_color, Color::clWhite()));
		$this->addDataSerie($dataset, $color, $name);
	}

	/**
	 * Returns the visibility of axes
	 * @param int $which use either X_AXIS|Y_AXIS to specify for which axis you query the visibility
	 * @return true if the specified axis is visible, false otherwise 
	 */
	function getAxisVisible($which) {
		return $which == X_AXIS ? $this->x_axis_visible : $this->y_axis_visible;
	}

	/**
	 * Returns the canvas area background color
	 * @return the index of the color resource that is used for the canvas area background
	 */
	function getBgColor() {
		return $this->bg_color;
	}

	/**
	 * Returns the refference to the image resource created with GD function imagecreate.
	 * It represents the canvas where we draw our chart/graphic elements.
	 * 
	 * @return the imagecreate refference to the object created
	 */
	function getCanvas() {
		return $this->canvas;
	}

	/**
	 * Returns a DrawArea object that contains the width|height for the canvas area
	 * @return DrawArea
	 */
	function getCanvasArea() {
		return $this->canvasArea;
	}

	/**
	 * Returns a Color object that is the container of all colors defined/used within current canvas.
	 * @return Color
	 */
	function getColors() {
		return $this->colors;
	}

	/**
	 * Returns the current FontDefinition we use to print text on canvas.
	 * @return FontDefinition
	 */
	function getCurrentFont() {
		return $this->currentFont;
	}

	/**
	 * Returns the DataSeries container that holds all the data series of this chart 
	 * @return DataSeries
	 */
	function getDataSeries() {
		return $this->dataSeries;
	}

	/**
	 * Returns a DrawArea object that contains the margins|size for the drawing area
	 * @return DrawArea
	 */
	function getDrawArea() {
		return $this->drawArea;
	}

	/**
	 * Returns the drawing area background color
	 * @return the index of the color resource that is used for the drawing area background
	 */
	function getDrawAreaBgColor() {
		return $this->drawArea_bg_color;
	}

	/**
	 * Returns the canvas area foreground color
	 * @return the index of the color resource that is used for the canvas area foreground color
	 */
	function getFgColor() {
		return $this->fg_color;
	}

	/**
	 * Returns grid visibility
	 * @return true if the grid is visible, false otherwise
	 */
	function getGridVisible() {
		return $this->grid_visible;
	}

	/**
	 * Returns the legend visibility
	 * @return true if the legend should be visible, false otherwise
	 */
	function getLegendVisible() {
		return $this->legend_visible;
	}

	/**
	 * Returns a DrawArea object that contains the margins for the drawing area
	 * @return DrawArea
	 */
	function getMargins() {
		return $this->marginArea;
	}

	/**
	 * Set the minimal value shown on the value (Y) scale or the null point (origin)
	 * It allows us to scale properly the Y axis
	 * @param float $null_point the minimal value shown on Y scale
	 * the Y-values are scaled down/up depending on this value
	 * I recommend to use 0:bar, min_value:points/lines
	 */
	function setNullPoint($null_point) {
		$this->null_point = $null_point;
	}

	/**
	 * Returns the axes orientation (vertical|horizontal)
	 * @return true if axes are oriented inversely (horizontal), false otherwise (vertical)
	 */
	function getReverseAxis() {
		return $this->reverse_axis;
	}

	/**
	 * Herper function for retrieving the bounding box for a text
	 *
	 * @param string $text : the text for which the boundings are calculated
	 * @param bool $bold : true if the text is bold, false otherwise
	 * @param int $angle : the angle (in degrees, where 0 means normal text) for the measured text
	 * @return multitype: array with the values of the bounding box
	 */
	function getTextBox($text, $bold = false, $angle = 0) {
		return imagettfbbox($this->currentFont->getFontSize(), $angle,
				$bold ? $this->currentFont->getFontBold() : $this->currentFont->getFontRegular(), $text);
	}

	/**
	 * Helper function that retrieves the height of the bounding box for a text
	 *
	 * @param string $text : the text for which to retrieve the height
	 * @param bool $bold : true if the text is bold
	 * @param int $angle : the angle (in degrees) for the text
	 * @return the number of pixels of the text height
	 */
	function getTextHeight($text, $bold = false, $angle = 0) {
		$text_rect = $this->getTextBox($text, $bold, $angle);
		return $text_rect[1] - $text_rect[5];
	}

	/**
	 * Helper function that retrieves both the width/height of the bounding box for a text
	 *
	 * @param string $text : the text for which to retrieve the height
	 * @param bool $bold : true if the text is bold
	 * @param int $angle : the angle (in degrees) for the text
	 * @return an 2-item array containing the number of pixels of the text width/height
	 */
	function getTextRect($text, $bold = false, $angle = 0) {
		$text_rect = $this->getTextBox($text, $bold, $angle);
		return array($text_rect[4] - $text_rect[0], $text_rect[1] - $text_rect[5]);
	}

	/**
	 * Helper function that retrieves the width of the bounding box for a text
	 *
	 * @param string $text : the text for which to retrieve the width
	 * @param bool $bold : true if the text is bold
	 * @param int $angle : the angle (in degrees) for the text
	 * @return the number of pixels of the text width
	 */
	function getTextWidth($text, $bold = false, $angle = 0) {
		$text_rect = $this->getTextBox($text, $bold, $angle);
		return $text_rect[4] - $text_rect[0];
	}

	/**
	 * Returns the title of the chart
	 * @return string
	 */
	function getTitle() {
		return $this->title;
	}

	/**
	 * Returns the tooltip visibility
	 * @return bool true if the tooltips are visible, false otherwise
	 */
	function getTooltipVisible() {
		return $this->tooltip_visible;
	}

	/**
	 * Checks the all serie's values to see if there is a turn/null point  (i.e. we have both, 
	 * negative and positive values so there we assume that there exists a virtual turn/null point)
	 * 
	 * @return boolean true if there are both positive & negatives values, false otherwise
	 */
	function hasTurnPoint1() {
		list($min_value, $max_value) = $this->dataSeries->getMinMaxValue();
		return $this->hasTurnPoint($min_value, $max_value);
	}

	/**
	 * Check for a specified range if there us a turn/null point within it (i.e. we have both, 
	 * negative and positive values so there we assume that there exists a virtual turn/null point)
	 * 
	 * @param float $min_value the minimum value that is compared with the maximum value
	 * @param float $max_value the maximum value that is compared with the minimum value
	 * @return boolean true if there are both positive & negatives values, false otherwise
	 */
	function hasTurnPoint($min_value, $max_value) {
		return ($min_value < 0 ? -1 : 1) != ($max_value < 0 ? -1 : 1);
	}

	/**
	 * Helper function to print out an generic error message in case of invalid data
	 */
	function invalidData() {
		$err = "INVALID DATA OR EMPTY DATASET";
		$this
				->printText($this->canvasArea->getWidth() / 2 - ($this->getTextWidth($err, true)) / 2,
						$this->canvasArea->getHeight() / 2, $err, true);
	}

	/**
	 * Draw the chart elements: contour, axis, title, grid, plot the data
	 */
	function paint($filename = null) {
		$this->init();

		$cw = $this->canvasArea->getWidth();
		$ch = $this->canvasArea->getHeight();

		$dw = $this->drawArea->getWidth();
		$dh = $this->drawArea->getHeight();

		imagefilledrectangle($this->canvas, 0, 0, $cw, $ch, $this->colors->createColor1($this->bg_color));
		imagefilledrectangle($this->canvas, $this->marginArea->getLeft(), $this->marginArea->getTop(),
				$this->marginArea->getLeft() + $dw, $this->marginArea->getTop() + $dh,
				$this->colors->createColor1($this->drawArea_bg_color));
		imagerectangle($this->canvas, 2, 2, $cw - 4, $ch - 4, $this->colors->createColor1($this->drawArea_bg_color));

		$this->printTitle();

		$this->drawXYAxes($this->reverse_axis);

		$this->drawGrid();

		$this->drawChart();

		if ($this->dataSeries->getCount() > 1)
			$this->printLegend();

		$this->printLibName();

		imagepng($this->canvas, $filename);
	}

	/**
	 * Print a serie name/color item within the legend
	 *
	 * @param (width,height) $rect : 2-item array that tells the width/height of the legend item
	 * @param string $name : the text to be printed as the serie name
	 * @param color $color : the color resource index used for legend item
	 * @param int $index : the 0-index of item in legend
	 */
	function printLegendItem($rect, $name, $color, $index) {
		if (!$this->legend_visible)
			return;
		list($tw, $th) = $rect;
		$right = $this->canvasArea->getWidth() - $tw - 26;
		$ly = 10 + 16 * $index;
		imagefilledrectangle($this->canvas, $right, $ly, $right + 10, $ly + 10, $color);
		$this->printText($right + 16, $ly + $th, $name, false, 0, $this->colors->createColor1($this->fg_color));
		imagerectangle($this->canvas, $right, $ly, $right + 10, $ly + 10, $this->colors->getColor1(Color::clGrey()));
	}

	/**
	 * Print the logo on the bottom-right part of the chart
	 */
	function printLibName() {
		$logo = "Powered by \"Poor man's PHP chart library\" - build " . LIB_VERSION;
		list($tw, $th) = $this->getTextRect($logo);
		if (!($this->canvasArea->getWidth() > $tw && $this->canvasArea->getHeight() > $th)) {
			$logo = "PHPCL ver " . LIB_VERSION;
			list($tw, $th) = $this->getTextRect($logo);
		}
		if ($this->canvasArea->getWidth() > $tw && $this->canvasArea->getHeight() > $th)
			$this->printText(($this->canvasArea->getWidth() - $tw) / 2, $this->canvasArea->getHeight() - $th, $logo);
	}

	/**
	 * Helper function for printing text in chart
	 *
	 * @param int $x : the X coordinate where the text is printed
	 * @param int $y : the Y coordinate where the text is printed
	 * @param string $text : the text being printed
	 * @param bool $bold : true if bold font must to be used, false otherwise
	 * @param int $angle : the angle (in degrees) to print the text (0 means normal)
	 * @param colorId $color : the color resource used to print the text
	 */
	function printText($x, $y, $text, $bold = false, $angle = 0, $color = -1) {
		$color = $color < 0 ? $this->colors->createColor1($this->fg_color) : $color;
		imagettftext($this->canvas, $this->currentFont->getFontSize(), $angle, $x, $y, $color,
				$bold ? $this->currentFont->getFontBold() : $this->currentFont->getFontRegular(), $text);
	}

	/**
	 * Set the true color & antialiased properties of the GD canvas
	 * @param bool $antialiased when true then use true color and antialiased drawing functionality, false otherwise
	 */
	function setAntialiased($antialiased) {
		$this->antialiased = $antialiased;
		$this->setSize($this->canvasArea->getWidth(), $this->canvasArea->getHeight(), true);
	}

	/**
	 * Set the name/caption for each axes
	 * @param string $x_axis_name the name for X-axis
	 * @param string $y_axis_name the name for Y-axis
	 * @param RGB $color the (r,g,b) color for axes names
	 */
	function setAxesNames($x_axis_name, $y_axis_name, $color = null) {
		$this->x_axis_name = $x_axis_name;
		$this->y_axis_name = $y_axis_name;
		if (!empty($color))
			$this->axes_fg_color = $color;
	}

	/**
	 * Set the axes visibility
	 * @param bool $x_axis_visible true if the X-axis should be visible, false otherwise
	 * @param bool $y_axis_visible true if the Y-axis should be visible, false otherwise
	 */
	function setAxisVisible($x_axis_visible, $y_axis_visible) {
		$this->x_axis_visible = $x_axis_visible;
		$this->y_axis_visible = $y_axis_visible;
	}

	/**
	 * Set the background color of the canvas area 
	 * @param int $bg_color the resource Id of the background color for the canvas area
	 */
	function setBgColor($bg_color) {
		$this->bg_color = $bg_color;
	}

	/**
	 * Set the background color of the canvas area  
	 * @param int $drawArea_bg_color the resource Id of the background color for the drawing area
	 */
	function setDrawAreaBgColor($drawArea_bg_color) {
		$this->drawArea_bg_color = $drawArea_bg_color;
	}

	/**
	 * Set the foreground color of the canvas area
	 * @param int $fg_color the resource Id of the foreground color for the canvas area
	 */
	function setFgColor($fg_color) {
		$this->fg_color = $fg_color;
	}

	/**
	 * Set the font definition to be used for printing text on canvas
	 * @param string $name the name of the font to be used
	 * @param bool $repaint if true the repaint the whole chart after setting the new font
	 */
	function setFont($name, $repaint = true) {
		$this->currentFont = $this->fontList->getFontByName($name);
		if ($repaint)
			$this->paint();
	}

	/**
	 * Set the visibility of the chart grid
	 * @param bool $visible when true then the grid will be made visible, false otherwise
	 */
	function setGridVisible($visible) {
		$this->grid_visible = $visible;
	}

	/**
	 * Set the visibility of the legend
	 * @param bool $visible if true then the legend will be made visible, false otherwise
	 */
	function setLegendVisible($visible) {
		$this->legend_visible = $visible;
	}

	/**
	 * Set the distance between the canvas and its inner drawing area
	 * @param int $top the distance between the canvas and the drawing area at the top
	 * @param int $bottom the distance between the canvas and the drawing area at the bottom
	 * @param int $left the distance between the canvas and the drawing area at the left
	 * @param int $right the distance between the canvas and the drawing area at the right
	 */
	function setMargins($top, $bottom, $left, $right) {
		$this->marginArea->setMargins($top, $bottom, $left, $right);
		$this->calcDrawRect();
	}

	/**
	 * Set the axes orientation (vertical|horizontal)
	 * @param bool $reverse if true then the axes are supposed to be reversed (i.e. keys on Y, values on X => horizontal), false otherwise (vertical)
	 */
	function setReverseAxis($reverse = false) {
		$this->reverse_axis = $reverse;
	}

	/**
	 * Set the chart size
	 * @param int $width
	 * @param int $height
	 */
	function setSize($width, $height, $force_recreate = false) {
		$recreate = $this->canvas > 0;

		// nothing changed, return
		if (!$force_recreate && $recreate && imagesx($this->canvas) == $width && imagesy($this->canvas) == $height)
			return;

		if ($this->antialiased)
			$new_canvas = imagecreatetruecolor($width, $height);
		else
			$new_canvas = imagecreate($width, $height);

		if ($recreate) {
			imagepalettecopy($new_canvas, $this->canvas);//copy the old color palette
			imagedestroy($this->canvas);//destroy the old canvas
		}

		$this->canvas = $new_canvas;//set the current canvas to the new created

		if (PHP_COMPILED_WITH_GD == true)
			call_user_func("imageantialias", $this->canvas, $this->antialiased);

		$this->canvasArea->setRect($width, $height);//set (logical) canvas area size

		// only when is created for the first time,
		// otherwise we have already copied the old palette
		if (!$recreate || $force_recreate)
			$this->initColors();
		else
			//just notify the color control about the new created canvas resource
			$this->colors->setCanvas($this->canvas);

		$this->calcDrawRect();
	}

	/**
	 * Set the title used for the chart
	 * @param string $title
	 */
	function setTitle($title, $color = null) {
		$this->title = $title;
		if (!empty($color))
			$this->title_fg_color = $color;
	}

	/**
	 * Set the visibility of the tooltips for the chart.
	 * A tooltip is the value of an item that is shown on the top of its. drawing (bar|point|slice etc)
	 * @param bool $visible if true then the tooltip are made visible, false otherwise
	 */
	function setTooltipVisible($visible) {
		$this->tooltip_visible = $visible;
	}

	/**
	 * Calculates how many values are too closed to eachother
	 * such that we can skip them from printing on the scale.
	 *
	 * @param array $values : the array of values
	 * @return number
	 */
	function skippedValuesCount($values) {
		$skip = 0;
		$lv = null;
		foreach ($values as $v) {
			if (!empty($lv))
				if (abs($v - $lv) <= 1)
					$skip++;
				else
					$lv = $v;
		}
		return $skip;
	}

	/**
	 * Detects the decimals for a number and format it acordingly
	 * 
	 * @param float $number : the number to be formatted
	 * @param int $maxDecimals : the max. number of decimals 
	 * @return a string representation of the formatted number
	 */
	private function autoFormat($number, $maxDecimals = 1) {
		$decimals = $number - floor($number);
		if ($decimals != 0)
			return sprintf("%.{$maxDecimals}f", $number);
		else
			return $number;
	}

	/**
	 * Calculates the chart' drawing area
	 */
	private function calcDrawRect() {
		$this->drawArea
				->setRect($this->canvasArea->getWidth() - $this->marginArea->getLeft() - $this->marginArea->getRight(),
						$this->canvasArea->getHeight() - $this->marginArea->getTop() - $this->marginArea->getBottom(),
						$this->canvasArea->getHeight() - $this->marginArea->getBottom(), $this->marginArea->getTop());
	}

	/**
	 * Draw the chart's X-Y axis
	 */
	private function drawXYAxes($reverse) {
		//TODO axis and values should not exceed the margins
		imagesetthickness($this->canvas, 2);
		if ($this->x_axis_visible) {
			imageline($this->canvas, $this->marginArea->getLeft(),
					$this->canvasArea->getHeight() - $this->marginArea->getBottom(),
					$this->canvasArea->getWidth() - $this->marginArea->getRight(),
					$this->canvasArea->getHeight() - $this->marginArea->getBottom(), $this->colors->getColor1(Color::clGrey()));
			$this->drawAxis(X_AXIS);
			$this->drawAxisName(X_AXIS);
		}
		if ($this->y_axis_visible) {
			imageline($this->canvas, $this->marginArea->getLeft(), $this->marginArea->getTop(), $this->marginArea->getLeft(),
					$this->canvasArea->getHeight() - $this->marginArea->getBottom(), $this->colors->getColor1(Color::clGrey()));
			$this->drawAxis(Y_AXIS);
			$this->drawAxisName(Y_AXIS);
		}
	}

	/**
	 * This adjusts the chart margins in order to print properly the scale values
	 */
	private function fixMargins() {
		if (!$this->getReverseAxis()) {
			$sname = $this->dataSeries->getLargestSerieName();
			// fix the bottom margin when there is not enough space to print the series names
			$th = 10 + $this->getTextHeight($sname, false, -45);
			if ($this->marginArea->getBottom() < $th)
				$this
						->setMargins($this->marginArea->getTop(), $th, $this->marginArea->getLeft(),
								$this->marginArea->getRight());
		} else {
			list($sname) = $this->dataSeries->getMinMaxKey();
			// fix the bottom margin when there is not enough space to print the series names
			$text_rect = $this->getTextBox($sname, false, 0);
			$tw = 10 + $this->getTextWidth($sname, false, 0);

			if ($this->marginArea->getLeft() < $tw)
				$this
						->setMargins($this->marginArea->getTop(), $this->marginArea->getBottom(), $tw,
								$this->marginArea->getRight());
		}
	}

	/**
	 * Initialize the color used by default by the image resource
	 */
	private function initColors() {
		if (!$this->canvas)
			return;
		$this->colors = new Color($this->canvas);
	}

	/**
	 * Draw the chart's title
	 */
	private function printTitle() {
		list($tw, $th) = $this->getTextRect($this->title);
		$title_x = ($this->canvasArea->getWidth() - $tw) / 2;
		$title_y = ($this->marginArea->getTop() + $th) / 2;
		$this->printText($title_x, $title_y, $this->title, true, 0, $this->colors->createColor1($this->title_fg_color));
	}

	private function drawAxisName($axeKind) {
		if ($axeKind == X_AXIS) {
			$value_head = $this->marginArea->getTop() + $this->drawArea->getHeight();//print the values from the bottom to top (on Y-axis)
			$aName = $this->x_axis_name;
		} else {
			$value_head = $this->marginArea->getLeft();//print the values from the left to right (on the X-axis)
			$aName = $this->y_axis_name;
		}

		// print out the axis name
		if (!empty($aName)) {
			$aColor = $this->colors->createColor1($this->axes_fg_color);
			if ($axeKind == X_AXIS) {
				$value_width = $this->getTextHeight($aName, true);
				$key_coord = ($this->canvasArea->getWidth() - $value_width) / 2;
				$value_coord = $value_head + 10 + $this->marginArea->getBottom() / 2;
				$this->printText($key_coord, $value_coord, $aName, true, 0, $aColor);
			} else {
				$this
						->printText($value_head - 2 * $this->marginArea->getLeft() / 3,
								$this->canvasArea->getHeight() / 2 + ($this->getTextHeight($aName, true, 90)) / 2, $aName, true,
								90, $aColor);
			}
		}
	}

	/**
	 * Print out the X|Y axes along with their keys/values.
	 * <br>
	 * It's a 100+ lines compressed function (bad coding!) but it does its job well,
	 * it's heavily commented, it's bug free (?) so there is no need for debugging, though.
	 *
	 * @param int $axeKind : X_AXIS(or 0)=draw the scale on X-axis|Y_AXIS(or 1)=draw it on Y-axis
	 */
	protected function drawAxis($axeKind) {
		if (!($axeKind == X_AXIS ? $this->x_axis_visible : $this->y_axis_visible))
			return;

		if ($axeKind == X_AXIS)
			$showWhat = $this->reverse_axis ? GRP_VALUE : GRP_KEY;
		else
			$showWhat = $this->reverse_axis ? GRP_KEY : GRP_VALUE;

		$values = $this->dataSeries->getGroupedItems($showWhat, $showWhat == GRP_VALUE);//read the axis keys/values

		$count = count($values);
		if ($count < 1)
			return;

		imagesetthickness($this->canvas, 0);
		$color = $this->colors->getColor1(Color::clGrey());
		$ptr_color = $this->colors->createColor1(Color::clGrey());

		if ($axeKind == X_AXIS) {
			$key_head = $this->marginArea->getLeft();//start printing keys from left to right (on the bottom/X-axis)
			$value_head = $this->marginArea->getTop() + $this->drawArea->getHeight();//print the values from the bottom to top (on Y-axis)
		} else {
			$key_head = $this->marginArea->getTop() + $this->drawArea->getHeight();//start printing the keys from the bottom to up (on the left/Y-axis)
			$value_head = $this->marginArea->getLeft();//print the values from the left to right (on the X-axis)
		}
		$max_value = $values[count($values) - 1];

		$i = 0;
		// fix those situations when we deal with a NAN value
		while ($showWhat == GRP_VALUE && is_nan($values[$i]))
			$i++;
		$min_value = $values[$i];

		$hasTurnPoint = $this->hasTurnPoint($min_value, $max_value);//if true then we have mixed values (+/-)

		list($scale, $offset) = $this->getScale($axeKind + 1, $this->null_point);//calculate the scaling factor

		$angle = 0;//the angle we use to print the values on axis; by default normal text
		$value_height = $axeKind == X_AXIS ? $this->getTextWidth($max_value . ".1") : $this->getTextHeight($max_value . ".1");//req. to calculate the max. value text width

		// calculate how many $values fit on the scale; in case they don't fit all
		// then find out how to distribute these $values on the scale
		$L = $axeKind == X_AXIS ? $this->drawArea->getWidth() : $this->drawArea->getHeight();//the length of the scale, where keys/values are printed
		$scale_steps_ = 1 + $L / $value_height;//how many steps we afford
		$scale_steps = min($scale_steps_, $count);//how many steps we really need (base on our $values)

		// if not enough space then try to print them oblique so that we gain some space
		if ($scale_steps < $count) {
			$possibleSkipped = ($showWhat == GRP_VALUE) ? $this->skippedValuesCount($values) : 0; // how many values are closed to each other so that we don't take into the account all of them

			if ($showWhat == GRP_KEY || ($showWhat == GRP_VALUE && $possibleSkipped < $count)) {
				$value_height = $this->getTextHeight($max_value . ".1");
				$scale_steps_ = 1 + $L / $value_height;//how many steps we can afford
				$scale_steps = min($scale_steps_, $count - $possibleSkipped);//how many steps we really need
				$angle = 45;//set the angle to 45 that we'll print on 45 degrees
			}
		}

		// the step as pixels
		if ($showWhat == GRP_KEY)
			$stepWidth = $L / $scale_steps;

		$skipEveryN = $count / $scale_steps;//skip every n $values so that we use how much we can, but maybe not all

		$j = 0;//keep the track of the values printed on the scale
		$lHead = $axeKind == X_AXIS ? $value_head : $key_head;
		$sign = $axeKind == X_AXIS ? 1 : -1;

		for ($i = 0; $i < $count; $i++) {
			$value = $values[$i];
			if ($showWhat == GRP_VALUE)
				$value = $this->autoFormat($value, DECIMALS);
			if ($i >= $j * $skipEveryN/*if we've skipped enough values*/ || $i + 1 == $count/*or we reached the end*/) {
				// print the $value on the scale
				list($value_width, $value_height_) = $this->getTextRect($value);
				if ($angle > 0)
					$value_height = $value_width * TrigonometryTable::$SinT[$angle] / 1024;
				else
					$value_height = $value_height_;

				//in case we print at 45deg. recalculate the text height
				$value_coord = $value_head + $sign * ($axeKind == X_AXIS ? $value_height : $value_width + ($angle > 0 ? 2 : 6));
				if ($showWhat == GRP_VALUE) {
					$key_coord = $key_head + $sign * ($values[$i] - $this->null_point) * $scale
							- $sign * ($hasTurnPoint ? $offset * $scale : 0);
				} else {
					$key_coord = $key_head + $sign * min($L, $stepWidth * ($count < $scale_steps ? 0.5 : 0 + $j));
					if ($scale_steps_ > $count)
						$key_coord += $sign * $stepWidth / 2;
				}

				// is the size (width/height) of the value that has to be printed on the scale
				if ($angle > 0)
					$k = ($axeKind == X_AXIS ? $value_height_ : $value_width) * (TrigonometryTable::$CosT[$angle] / 1024);
				else
					$k = $axeKind == X_AXIS ? $value_width : $value_height_;

				// if last printed unit on the scale is close enough to the current one
				// then skip it so that we don't overlap it (better fewer than mumbo-jambo)
				if ($key_coord != $lHead && abs($key_coord - $lHead) < $k + 5) {
					$j++;
					continue;
				}

				// print the text on the scale
				if ($axeKind == X_AXIS) {
					if ($key_coord >= $this->marginArea->getLeft()
							&& $key_coord <= $this->marginArea->getLeft() + $this->drawArea->getWidth()) {
						imageline($this->canvas, $key_coord, $value_head, $key_coord, $value_head + 4, $ptr_color);
						$this
								->printText($key_coord - $value_width / 2,
										$value_head + $value_height * ($angle == 0 ? 2 : 1) + 10 * ($angle == 0 ? 0 : 1), $value,
										false, $angle);
					}
				} else if ($key_coord >= $this->marginArea->getTop()
						&& $key_coord <= $this->marginArea->getTop() + $this->drawArea->getHeight()) {
					imageline($this->canvas, $value_head, $key_coord, $value_head - 4, $key_coord, $ptr_color);
					$this->printText($value_coord, $key_coord + $value_height * ($angle == 0 ? 0.5 : 1), $value, false, $angle);
				}

				$lHead = $key_coord;
				$j++;//increment by 1 so that we are going to skip the next $skipEveryN $values
			}
		}

		// print out the origin/zero value, if it's necessary
		if ($showWhat == GRP_VALUE && $count > 0 && !in_array($this->null_point, $values)
				&& !in_array($this->null_point - 1, $values) && !in_array($this->null_point + 1, $values)) {
			list($value_width, $value_height) = $this->getTextRect($this->null_point);
			$key_coord = $key_head - $sign * ($hasTurnPoint ? $offset * $scale : $this->null_point);
			$value_coord = $value_head + $sign * ($axeKind == X_AXIS ? $value_height : $value_width + ($angle > 0 ? 2 : 6));
			if ($axeKind == X_AXIS) {
				imageline($this->canvas, $key_coord, $value_head, $key_coord, $value_head + 4, $color);
				$this
						->printText($key_coord - $value_width / 2, $value_head + $value_height * ($angle == 0 ? 2 : 0.5),
								$this->null_point, false, $angle);
			} else {
				imageline($this->canvas, $value_head, $key_coord, $value_head - 4, $key_coord, $color);
				$this
						->printText($value_coord, $key_coord + $value_height * ($angle == 0 ? 0.5 : 1), $this->null_point, false,
								$angle);
			}
		}
	}

	/**
	 * The method to be implemented by the child class to draw the chart
	 */
	protected function drawChart() {
	}

	/**
	 * Draw the chart grid 
	 */
	protected function drawGrid() {
		if (!$this->grid_visible)
			return;

		imagesetthickness($this->canvas, 0);
		$color = $this->colors->getColor1(Color::clSilver());

		if ($this->reverse_axis) {
			$l = $this->drawArea->getWidth();
			$L = $this->drawArea->getHeight() - 1;
			$from = $this->marginArea->getLeft() + GRID_STEP;
			$to = $this->marginArea->getLeft() + $l;
			$start = $this->marginArea->getTop();
		} else {
			$l = $this->drawArea->getHeight();
			$L = $this->drawArea->getWidth();
			$from = $this->marginArea->getTop() + $l - GRID_STEP;
			$to = $this->marginArea->getTop();
			$start = $this->marginArea->getLeft();
		}

		for ($i = $from; ($this->reverse_axis && $i <= $to) || (!$this->reverse_axis && $i >= $to); $i += GRID_STEP
				* ($this->reverse_axis ? 1 : -1)) {
			if ($this->reverse_axis) {
				$x1 = $i;
				$y1 = $this->marginArea->getTop() + 1;
				$x2 = $x1;
				$y2 = $this->marginArea->getTop() + $L;
			} else {
				$x1 = $this->marginArea->getLeft() + 1;
				$y1 = $i;
				$x2 = $this->marginArea->getLeft() + $L;
				$y2 = $y1;
			}
			imageline($this->canvas, $x1, $y1, $x2, $y2, $color);
		}

		list($min_value, $max_value) = $this->dataSeries->getMinMaxValue();
		$hasTurnPoint = $this->hasTurnPoint($min_value, $max_value);

		// draw the null point grid line if sign(min) != sign(max)
		if ($hasTurnPoint) {
			list($scale, $offset) = $this->getScale();

			if ($this->reverse_axis) {
				$null_point = $this->marginArea->getLeft() - $min_value * $scale;
				$x1 = $null_point;
				$y1 = $start;
				$x2 = $x1;
				$y2 = $y1 + $L;
			} else {
				$null_point = $this->drawArea->getTop() + $min_value * $scale;
				$x1 = $start;
				$y1 = $null_point;
				$x2 = $x1 + $L;
				$y2 = $y1;
			}

			// draw a line that intersects the graph origin
			imagesetthickness($this->canvas, 2);
			imageline($this->canvas, $x1, $y1, $x2, $y2, $this->colors->getColor1(Color::clGrey()));
		}
	}

	/**
	 * Calculates and return the scale factor for the chart being plotted
	 * @param int $calcMethod 0=auto-scale;1=force X-axis;2=force Y-axis
	 * @return 2-item array containing the scale value and the offset from the minimal value to the null-point (if exists)
	 */
	protected function getScale($calcMethod = 0, $null_point_value = 0) {
		$key_type = $this->dataSeries->getSerie(0)->getDatasetKeyType();
		$reverse = $this->reverse_axis;
		switch ($calcMethod) {
		case 0://auto
			$areaHeight = $reverse ? $this->drawArea->getWidth() : $this->drawArea->getHeight();// helps us to calculate how tall a bar could be
			list($min_value, $max_value) = $this->dataSeries->getMinMaxValue();
			break;
		case 1://X-axis
			$areaHeight = $this->drawArea->getWidth();
			list($min_value, $max_value) = $reverse ? $this->dataSeries->getMinMaxValue() : $this->dataSeries->getMinMaxKey();
			break;
		case 2://Y-axis
			$areaHeight = $this->drawArea->getHeight();
			list($min_value, $max_value) = $reverse ? $this->dataSeries->getMinMaxKey() : $this->dataSeries->getMinMaxValue();
			break;
		}

		// turn point is the origin of the chart where the + bars are drawn as opposed to the - bars
		$hasTurnPoint = $this->hasTurnPoint($min_value, $max_value);
		if ($hasTurnPoint)
			$offset = $min_value;
		// the distance from the minimum negative value to the null point (origin)
		else
			$offset = 0;// all values are either negative/positive so no null point need it

		if ($key_type != DATE_KEY) {
			$hValue = (abs($max_value) > abs($min_value) ? $max_value : $min_value);// highest absolute value
			$vScaleMaxValue = ($hasTurnPoint ? abs($hValue) : $hValue) - $offset;
		} else {
			$vScaleMaxValue = $max_value;
		}

		// the highest (absolute) value on scale
		if ($key_type == DATE_KEY && ((!$reverse && $calcMethod == 1) || ($reverse && $calcMethod == 2)))
			$diff = $this->dataSeries->getKeyDifference($vScaleMaxValue, $min_value);
		else
			$diff = $vScaleMaxValue - $null_point_value;

		$vScale = $areaHeight / ($diff == 0 ? 1 : $diff);// the vertical scale factor
		return array($vScale, $offset);
	}

	/**
	 * The method to be implemented by the child class to initialize 
	 * the class working variables before anything else, like drawing axes, 
	 * grid, chart, etc (e.g. the variable "null_point")
	 */ 
	protected function init() {

	}

	/**
	 * Print each serie name/color as a chart legend
	 */
	protected function printLegend() {
		if (!$this->legend_visible)
			return;
		$rect = $this->getTextRect($this->dataSeries->getLargestSerieName());
		imagesetthickness($this->canvas, 0);
		$index = 0;
		foreach ($this->dataSeries->getSeries() as $serie)
			$this->printLegendItem($rect, $serie->getName(), $this->colors->createColor1($serie->getColor()), $index++);
	}

}
?>
