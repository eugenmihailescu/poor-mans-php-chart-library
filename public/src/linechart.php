<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c3d47cbf895f7c1bb72593b3ca2ed43ee5941c33 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:50:03 2013 +0100 $
 * @file:		linechart.php $
 * 
 * @id:	linechart.php | Sun Jan 20 18:50:03 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'scatterchart.php';

/**
 * Class that provides the functionalities of a multi-serie line chart.
 * 
 * @author Eugen Mihailescu
 *
 */
class LineChart extends ScatterChart {

	function __construct() {
		parent::__construct();
		$this->setTitle("Line chart");
		$this->setConnectedPoints(true);//we want to connect the points with eachother, like a line
	}

	/**
	 * (non-PHPdoc)
	 * @see ScatterChart::drawChart()
	 */
	function drawChart() {
		$this->setSorted(true);
		parent::drawChart();
	}

	/**
	 * (non-PHPdoc)
	 * @see ScatterChart::drawGrid()
	 */
	function drawGrid() {
		parent::drawGrid();
	}

}
?>
