<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		22fc1970954e0e5720230854563f5a0631ab830a $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:38:20 2013 +0100 $
 * @file:		barcharth.php $
 * 
 * @id:	barcharth.php | Sun Jan 20 18:38:20 2013 +0100 | Eugen Mihailescu  $
 * 
 */
include_once 'barchartv.php';

/**
 * Class that implements the horizontal bar chart functionalities.
 * 
 * @author Eugen Mihailescu
 *
 */
class BarChartH extends BarChartV {

	function __construct() {
		parent::__construct();
		$this->setTitle("Horizontal bar chart");
		$this->setReverseAxis(true);
		$this->onSetDataset = array($this, "fixMargins");
	}
}
?>
