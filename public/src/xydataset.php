<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c52fa95ad62614ca7d78794ce3066487ac012b94 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 09:30:13 2013 +0100 $
 * @file:		xydataset.php $
 * 
 * @id:	xydataset.php | Sat Jan 26 09:30:13 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'xyvalue.php';

define("TEXT_KEY", 0);
define("NUMERIC_KEY", 1);
define("DATE_KEY", 2);

/**
 * Class that provides the dataset storage functionalities
 * 
 * @author Eugen Mihailescu
 *
 */
class XYDataSet {

	private $items;

	/**
	 * Default class constructor
	 */
	function __construct() {
		$this->items = array();
	}

	/**
	 * Add a new item to dataset specifying also its color
	 * @param XYValue $item
	 * @param array<r,g,b> $color
	 */
	function addColoredItem($item, $color) {
		$item->setColor($color);
		$this->items[] = $item;
	}

	/**
	 * Add a new item to dataset
	 * @param XYValue $item
	 */
	function addItem($item) {
		$this->items[] = $item;
	}

	/**
	 * Check if a string is a valid date
	 * @param string $date the date in yy(yy)-mm-dd format
	 * @return boolean true if is valid date, false otherwise
	 */
	function checkData($date) {
		if (substr_count($date, "-") == 2) {
			list($yy, $mm, $dd) = explode("-", $date);
			if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)) {
				return checkdate($mm, $dd, $yy);
			}
		}
		return false;
	}

	/**
	 * Returns the value coresponding to the first key with the specified name.
	 * @param general $key the key for which we'll return the value.
	 * @return number the value coresponding to the key
	 */
	function getValueForKey($key) {
		$result = null;
		foreach ($this->items as $xyItem)
			if (strcmp($xyItem->getKey(), $key) == 0) {
				$result = $xyItem->getValue();
				break;
			}
		return $result;
	}

	/**
	 * Returns the item at the specified index
	 * @param int $index
	 * @return a refference to the item
	 */
	function getItem($index) {
		return $this->items[$index];
	}

	/**
	 * The total count of elements within dataset
	 * @return number
	 */
	function getItemCount($positive_only = false, $roundingLen = 2) {
		if (!$positive_only)
			return count($this->items);
		$result = 0;
		foreach ($this->items as $item)
			$result += (round($item->getValue(), $roundingLen) > 0);
		return $result;
	}

	/**
	 * Returns the key type
	 * @return int Can be one of TEXT_KEY,NUMERIC_KEY or DATE_KEY.
	 */
	function getKeyType() {
		$result = null;
		if (count($this->items) > 0) {
			$key = $this->items[0]->getKey();
			if (strlen($key))
				if (!$this->checkData($key)) {
					if (is_numeric($key) === FALSE)
						$result = TEXT_KEY;
					else
						$result = NUMERIC_KEY;
				} else
					$result = DATE_KEY;
		}
		return $result;
	}

	/**
	 * Returns the largest key within dataset.
	 * @return the largest found key or null if there is no element within dataset.
	 */
	function getLargestKey() {
		$result = null;
		$max = -1;
		foreach ($this->items as $item) {
			$key = $item->getKey();
			$len = strlen($key);
			if ($len > $max) {
				$result = $key;
				$max = $len;
			}
		}
		return $result;
	}

	/**
	 * Returns the max value within all elements in dataset
	 * @return a number
	 */
	function getMaxKey() {
		$max = ~PHP_INT_MAX;
		foreach ($this->items as $item)
			$max = max($max, $item->getKey());
		return $max;

	}

	/**
	 * Returns the max value within all elements in dataset
	 * @param string $key search only for specified key or for any when $key is null 
	 * @return a number
	 */
	function getMaxValue($key = null) {
		$max = ~PHP_INT_MAX;
		foreach ($this->items as $item)
			if (empty($key) || strcmp($item->getKey(), $key) == 0)
				$max = max($max, $item->getValue());

		return $max;

	}

	/**
	 * Returns the max value within all elements in dataset
	 * @return a number
	 */
	function getMinKey() {
		$min = PHP_INT_MAX;
		foreach ($this->items as $item)
			$min = min($min, $item->getKey());
		return $min;

	}
	/**
	 * Returns the min value within all elements in dataset
	 * @param string $key search only for specified key or for any when $key is null 
	 * @return a number
	 */
	function getMinValue($key = null) {
		$min = PHP_INT_MAX;
		foreach ($this->items as $item)
			if (empty($key) || strcmp($item->getKey(), $key) == 0)
				$min = min($min, $item->getValue());
		return $min;
	}

	/**
	 * Sorts the dataset by key
	 * 
	 * @return XYDataSet the sorted dataset
	 */
	function getSortedData() {

		$resultSet = new XYDataSet();
		$value_array = array();

		// create a temp $value_array that holds text/value pairs
		foreach ($this->items as $i)
			$value_array[$i->getValue()] = array($i->getKey(), $i->getColor());

		// sort the $value_array by its value, which is the "text"
		asort($value_array);

		// create back a new XYDataset that contains the sorted $values
		reset($value_array);
		while (list($index, $val) = each($value_array)) {
			list($key, $color) = $val;
			$item = new XYValue($key, $index);
			$item->setColor($color);
			$resultSet->addItem($item);
		}
		return $resultSet;
	}

	/**
	 * Calculates and return the sum of the values stored within dataset
	 * @param bool $positive_only if true then sum only the positive values, ignoring the negatives one
	 * @param int $roundingLen the number of digits used to round each values before adding to the final sum
	 * @return number
	 */
	function getTotalValue($positive_only = false, $roundingLen = 2) {
		$total = 0;
		foreach ($this->items as $item) {
			$v = $item->getValue();
			if (!$positive_only || round($v, $roundingLen) > 0)
				$total += $v;
		}
		return $total;
	}

	/**
	 * Returns an array with (key,value) pairs for each record within dataset
	 * @return array
	 */
	function getXYArray() {
		$result = array();
		foreach ($this->items as $item) {
			$result[] = array($item->getKey(), $item->getValue());
		}
		return $result;
	}
}
?>
