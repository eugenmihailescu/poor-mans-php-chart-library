<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		c3d47cbf895f7c1bb72593b3ca2ed43ee5941c33 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Jan 20 18:50:03 2013 +0100 $
 * @file:		lineareg.php $
 * 
 * @id:	lineareg.php | Sun Jan 20 18:50:03 2013 +0100 | Eugen Mihailescu  $
 * 
 */

/**
 * Helper class to linear regression some (x,y) points on a scattered graph.
 * 
 * @author Eugen Mihailescu
 * 
 * http://en.wikipedia.org/wiki/Regression_analysis
 * 	or, by example
 * http://www.wired.com/wiredscience/2011/01/linear-regression-by-hand/
 *
 */
class LinearRegression {
	private $items;

	/**
	 * The class constructor that supply the (x,y) points
	 * @param array $items : array [(X1,Y1),(X2,Y2),...,(Xn,Yn)]
	 */

	function __construct($items) {
		$this->items = $items;
	}

	/**
	 * We have to find the intercept "b" of the equation y=mx+b
	 * when we know a serie of points (Xi,Yi) with i=[0..n]
	 * that are scattered on a graphic.
	 *
	 *      Sy-m*Sx
	 * b = --------- , where m is the slope calculated by calcSlope()
	 *         n
	 * @return array of (m,b)
	 */
	function getGetEquation() {

		list($m, $n, $Sx, $Sy) = $this->calcSlope();

		$b = ($Sy - $m * $Sx) / $n;

		return array($m, $b);
	}

	/**
	 * We have to find the slope "m" of the equation y=mx+b
	 * when we know a serie of points (Xi,Yi) with i=[0..n]
	 * that are scattered on a graphic.
	 * 
	 *      n*Sxy-Sx*Sy
	 * m = -------------
	 *      n*Sx2-(Sx)^2
	 *      
	 * @return array of (m,n,Sx,Sy) where Sx = sum(Xi), Sy = sum(Yi)      
	 *      
	 */
	private function calcSlope() {
		$Sx = 0;
		$Sy = 0;
		$Sxy = 0;
		$Sx2 = 0;
		$n = 0;

		foreach ($this->items as $item) {
			list($x, $y) = $item;

			$Sx += $x;
			$Sy += $y;
			$Sxy += $x * $y;
			$Sx2 += $x * $x;
			$n++;
		}

		$div = $n * $Sx2 - $Sx * $Sx;
		$m = $div == 0 ? PHP_INT_MAX : (($n * $Sxy - $Sx * $Sy) / $div);

		return array($m, $n, $Sx, $Sy);

	}

}

?>
