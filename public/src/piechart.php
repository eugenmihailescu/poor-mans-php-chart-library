<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		7cf68f6f24b7cf8873b55521d71a2a2f9645554e $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Mon Jan 21 23:23:06 2013 +0100 $
 * @file:		piechart.php $
 * 
 * @id:	piechart.php | Mon Jan 21 23:23:06 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'basechart.php';
include_once 'trigtable.php';

define("HEIGHT_3D", 30);

/**
 * Class that implements the pie chart functionalities.
 * 
 * @author Eugen Mihailescu
 *
 */

class PieChart extends BaseChart {

	private $dataset;
	private $fgColor;
	private $margin_offset;

	function __construct() {
		parent::__construct();
		$this->setAxisVisible(false, false);
		$this->setGridVisible(false);
		$this->setTooltipVisible(true);
		$this->setTitle("3D-Pie chart");
		$this->setAntialiased(false); // we cannot handle well shadows when anti-alias is ON
		$this->margin_offset = 20;
	}

	/* (non-PHPdoc)
	 * @see BaseChart::DrawChart()
	 */
	function drawChart() {

		// take in consideration only the first series; multiple series not implemented (yet) for pie-chart
		if ($this->getDataSeries()->getCount() > 0)
			$this->dataset = $this->getDataSeries()->getSerie(0)->getDataset();
		$total = 0;
		if (!empty($this->dataset))
			$total = $this->dataset->getTotalValue(true);

		if ($total <= 0) {
			$this->invalidData();

			return;
		}

		if ($this->getLegendVisible())
			$vOffset = $this->getTopOffset();
		else
			$vOffset = 0;

		$top = $this->getMargins()->getTop() + $this->getDrawArea()->getHeight() / 2 + $vOffset;

		$palette = $this->getColors()
				->randomColors($this->dataset->getItemCount(), array($this->getColors()->clWhite(), $this->getBgColor()));

		$h3D = min(HEIGHT_3D, $this->getDrawArea()->getWidth() / 10);
		// draw 3D effect using the darker colors
		for ($i = $h3D; $i > 0; $i--)
			$this->drawPie($total, $top + $i, true, $palette);

		// draw the pie using the normal colors
		$this->drawPie($total, $top, false, $palette);

		$this->drawSlices($total, $top);
	}

	/**
	 * Draw an eliptical pie
	 * 
	 * @param float $total the total of serie's values; the pie percentage is reported to this
	 * @param int $cy the Y coordinate to draw the pie
	 * @param bool $dark true if the used color will be darker (for 3D effect)
	 * @param array $palette the container that provides the colors used within class
	 */
	private function drawPie($total, $cy, $dark, $palette) {
		$slice_count = $this->dataset->getItemCount();
		$start = 180;

		for ($i = 0; $i < $slice_count; $i++) {
			$value = 100 * $this->dataset->getItem($i)->getValue() / $total;
			$end = round($start + 3.6 * $value, 0);

			if ($value <= 0 || round($end - $start, 2) == 0)
				continue;

			// read the item's color; if no color defined then set a random one
			$color = $this->dataset->getItem($i)->getColor();
			if (empty($color)) {
				$color = $palette[$i];
				$this->dataset->getItem($i)->setColor($color);//..so that the legend can use it later
			}

			$color = $this->getColors()->createColor1($color);

			imagefilledarc($this->getCanvas(), $this->getMargins()->getLeft() + $this->getDrawArea()->getWidth() / 2, $cy,
					$this->getDrawArea()->getWidth() - 2 * $this->margin_offset, $this->getDrawArea()->getHeight() / 2, $start,
					$end, ($dark ? -1 : 0) + $color, IMG_ARC_PIE);

			$start = $end;
		}
	}

	/**
	 * Print the slice percentage just in the middle of the slice
	 *
	 * @param int $cx : the ellipse's center on X-axis
	 * @param int $cy : the ellipse's center on Y-axis
	 * @param int $width : the ellipse's width
	 * @param int $height : the ellipse's height
	 * @param int $start : the arc start angle, in degrees
	 * @param int $end : the arc end angle, in degrees; 0° is located at the three-o'clock position, and the arc is drawn clockwise
	 * @param float $value : the value of the slice used to calculate the slice's percentage
	 * @param color $color : the color of the slice
	 */
	private function drawSlice($cx, $cy, $width, $height, $start, $end, $value, $color) {

		/* fix start/end angles*/
		if ($start % 360 == $end % 360) {
			$start = 0;
			$end = 360;
		}
		$start = round($start, 0) % 360;
		$end = round($end, 0) % 360;
		while ($start < 0)
			$start += 360;
		while ($end < $start)
			$end += 360;
		if ($start == $end) {
			$start = 0;
			$end = 360;
		}
		$angle = ($start + ($end - $start) / 2) % 360;

		/*slice the pie*/
		if ($end - $start < 360) {
			$x = (TrigonometryTable::$CosT[$start % 360] * $width / (2 * 1024)) + $cx;
			$y = (TrigonometryTable::$SinT[$start % 360] * $height / (2 * 1024)) + $cy;
			imageline($this->getCanvas(), $cx, $cy, $x, $y, $color);
			imageline($this->getCanvas(), $cx + 1, $cy, $x + 1, $y, $color);
			if ($start < 180) {
				imageline($this->getCanvas(), $x, $y, $x, $y + 31, $color);
				imageline($this->getCanvas(), $x + 1, $y, $x + 1, $y + 31, $color);
			}
		}

		/*calc. the caption's coordinates*/
		if (!$this->getTooltipVisible())
			return;
		if ($end - $start < 360) {
			$x = 2 * (TrigonometryTable::$CosT[$angle] * $width / (2 * 1024)) / 3 + $cx;
			$y = 2 * (TrigonometryTable::$SinT[$angle] * $height / (2 * 1024)) / 3 + $cy;
		} else {
			// if a 100% slice then print the label in the center
			$x = $cx;
			$y = $cy;
		}

		list($tw, $th) = $this->getTextRect($value, true);

		if ($this->getEllipseArcLen($width, $height, ($end - $start) / 2) > abs($tw / 2)) {
			$this->printText($x - $tw / 2, $y + $th / 2, $value, true, 0, $color);
		}
	}

	/**
	 * Draw the value in the middle of the pie slices base on the item's text/value data
	 * @param float $total the total of serie's values; the pie percentage is reported to this
	 * @param int $cy the Y coordinate to draw the pie
	 */
	private function drawSlices($total, $cy) {
		if ($total <= 0)
			return;
		$slice_count = $this->dataset->getItemCount();

		$start = 180;
		$color = $this->getColors()->createColor1(Color::clWhite());

		$largest_text = $this->dataset->getLargestKey();
		list($tw, $th) = $this->getTextRect($largest_text, true);
		$legend_left = $this->getCanvasArea()->getWidth() - 30 - $tw;

		$cx = $this->getMargins()->getLeft() + $this->getDrawArea()->getWidth() / 2;
		$w = $this->getDrawArea()->getWidth() - 2 * $this->margin_offset;
		$h = $this->getDrawArea()->getHeight() / 2;

		$legitime_items = 0;
		for ($i = 0; $i < $slice_count; $i++) {
			$item = $this->dataset->getItem($i);
			$value = 100 * $item->getValue() / $total;
			$end = round($start + 3.6 * $value, 0) % 360;

			if ($value <= 0 || round($end - $start, 2) == 0)
				continue;
			$legitime_items++;

			$this->drawSlice($cx, $cy, $w, $h, $start, $end, sprintf("%.1f%%", $value), $color);
			$vcolor = $this->getColors()->getColor1($item->getColor());

			if ($this->getLegendVisible() && $legitime_items < $this->getDrawArea()->getHeight() / $th)
				$this->printLegend1($legend_left, $legitime_items, $item->getKey(), $vcolor);
			$start = $end;
		}
	}

	/**
	 * Calculates the length of the arc of ellipses determined by an specified angle
	 *
	 * @param int $width : the ellipse's width
	 * @param int $height : the ellipse's height
	 * @param int $angle : the angle which vector's endings determines an arc on ellipse
	 */
	private function getEllipseArcLen($width, $height, $angle) {
		$a = $width / 2;
		$b = $height / 2;
		$c = pi() * (3 * ($a + $b) - sqrt((3 * $a + $b) * ($a + 3 * $b)));
		return $c * $angle / 360;
	}

	/**
	 * Returns the number of pixels that the pie may move down in order to allow the legent to not overlap the pie. 
	 * @return number
	 */
	private function getTopOffset() {
		$result = 0;

		// calculate if legend overlaps the pie
		$itemCount = $this->dataset->getItemCount(true);
		$text_rect = $this->getTextBox("0", true);
		$th = ($text_rect[1] - $text_rect[5]) / 2;
		$legend_height = 10 + $itemCount * (4 + max(10, $th));

		$cy = $this->getMargins()->getTop() + $this->getDrawArea()->getHeight() / 2;

		//if overlaps then move it to bottom with $result pixels.
		$pie_top = $cy + TrigonometryTable::$SinT[270] * ($this->getDrawArea()->getHeight() / 2) / (2 * 1024);
		$pie_bottom = $cy + TrigonometryTable::$SinT[90] * ($this->getDrawArea()->getHeight() / 2) / (2 * 1024);

		$space_required = $legend_height - ($pie_top - 10);

		if ($space_required > 0) {
			$space_available = $this->getDrawArea()->getHeight() - $pie_bottom;
			if ($space_required < $space_available)
				$result = $space_required;
			else
				$result = $space_available;
		}
		return $result;
	}

	/**
	 * Print the legend on the top-right corner of the chart
	 *
	 * @param int $left : the left position of the legend
	 * @param int $index : the item's index being printed
	 * @param str $text : the item's text being printed
	 * @param color $color : the item's color being printed
	 */
	private function printLegend1($left, $index, $text, $color) {
		$th = $this->getTextHeight($text, true) / 2;
		$top = 10 + $index * (4 + max(10, $th));

		/*slice the pie*/
		$title_rect = $this->getTextBox($this->getTitle(), true);
		imagefilledrectangle($this->getCanvas(), $left, $top, $left + 10, $top + 10, $color);
		$this->printText($left + 16, $top + $this->getCurrentFont()->getFontSize(), $text);
	}

	/**
	 * (non-PHPdoc)
	 * @see BaseChart::printLegend()
	 */
	protected function printLegend() {
		// It's necessary to redeclare it here (even empty) because we have our own implementation.
		// By providing an empty implementation we make sure that the base class implementation
		// will not interfere with our implementation.
	}
}
?>
