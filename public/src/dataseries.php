<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of "Poor man's PHP chart library".
 * 
 *    Copyright 2013 "Eugen Mihailescu"
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *	http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.1a-32-gf6d3671 $
 * @commit:		7f3da0aab2426f68aac20be0ece7e6ba4878d84c $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Jan 26 14:18:28 2013 +0100 $
 * @file:		dataseries.php $
 * 
 * @id:	dataseries.php | Sat Jan 26 14:18:28 2013 +0100 | Eugen Mihailescu  $
 * 
 */

include_once 'dataserieitem.php';

/**
 * Rounding len of serie's values|numerical keys
 */
define("DECIMALS", 2);
define("CUSTOM_DATEDIFF", true);

/**
 * Class that provides the container for data serie items
 * 
 * @author Eugen Mihailescu
 *
 */
class DataSeries {

	private $dataSeries;

	function __construct() {
		$dataSeries = array();
	}

	/**
	 * Add a new serie to dataSerie collection
	 * 
	 * @param XYdataset $dataset : the dataset that represents the serie
	 * @param color $color : an array of RGB values that represents the color used for the serie
	 * @param string $name : the name of the serie
	 */
	function addSerie($dataset, $color, $name) {
		$this->dataSeries[] = new DataSerieItem($dataset, $color, $name);
	}

	/**
	 * Removes all defined data series
	 */
	function clearSeries() {
		while (count($this->dataSeries) > 0)
			unset($this->dataSeries[0]);
	}

	/**
	 * Return the number of the series
	 * @return number
	 */
	function getCount() {
		return count($this->dataSeries);
	}

	/**
	 * Retrieves the keys/values from all dataset
	 *
	 * @param int $what : GRP_VALUE (or 0) retrieves values;GRP_KEY (or 1) retrieves keys
	 * @param bool $sorted : when true sort the items, otherwise return them in their natural order
	 * @param int $sortOrder : 4=ASC; 3=DESC
	 * @return array of keys/values
	 */
	function getGroupedItems($what = 0, $sorted = false, $sortOrder = SORT_ASC) {
		$result = array();
		if (!empty($this->dataSeries))
			foreach ($this->dataSeries as $serie) {
				$count = $serie->getDataset()->getItemCount();

				for ($i = 0; $i < $count; $i++) {
					$xyValue = $serie->getDataset()->getItem($i);
					if ($what == 0) {
						$v = round($xyValue->getValue(), DECIMALS);
						if (is_nan($v))
							$v = 0;
					} else
						$v = $xyValue->getKey();

					if (!in_array($v, $result))
						$result[] = $v;
				}
			}

		if ($sorted)
			if ($sortOrder == SORT_ASC)
				sort($result);
			else
				rsort($result);
		return $result;
	}

	private function datediff($from, $to) {
		//PHP5.3: $days = $from->diff($to)->days + 1;
		$timestamp_from = (int) $from->format('U');
		$timestamp_to = (int) $to->format('U');
		return (int) round(($timestamp_to - $timestamp_from) / (60 * 60 * 24)) + 1;
	}

	/**
	 * Calculates the difference between two keys.
	 * It uses a difference method according to dataset's key type.
	 * @param object $key1 the key to substract from
	 * @param object $key2 the key that is substracted from $key1
	 * 
	 * @return the difference between keys. If DATE keys the difference represents
	 * the number of years,months,days,hours,minutes,seconds.
	 * If text/value key then it tries to substract like they are numbers.
	 * 
	 * @see PHP diff function
	 */
	function getKeyDifference($key1, $key2) {
		$result = null;
		if (!empty($this->dataSeries)) {
			$key_type = $this->dataSeries[0]->getDatasetKeyType();// it is supposed that all data series have the same key type, otherwise...
			switch ($key_type) {
			case TEXT_KEY:
				$result = $key1 - $key2;
				break;
			case NUMERIC_KEY:
				$result = $key1 - $key2;
				break;
			case DATE_KEY:
				$d1 = new DateTime($key1);
				$d2 = new DateTime($key2);
				if (CUSTOM_DATEDIFF)
					$result = $this->datediff($d2, $d1);
				else
					$result = $d2->diff($d1)->format("%r%a");
				break;
			}
		}
		return $result;
	}

	/**
	 * Returns the name of the largest serie name
	 * @return string the largest serie name
	 */
	function getLargestSerieName() {
		$result = null;
		$max = 0;
		$names = $this->getSerieNames();
		foreach ($names as $name) {
			$l = strlen($name);

			if ($l > $max) {
				$result = $name;
				$max = $l;
			}
		}
		return $result;
	}

	/**
	 * Returns the global min&max keys within all dataseries
	 * @return array with 2 elements: (min,max) keys
	 */
	function getMinMaxKey() {
		$key_type = $this->dataSeries[0]->getDatasetKeyType();
		switch ($key_type) {
		case TEXT_KEY:
			$min = "";
			$max = "Z";
			break;
		case NUMERIC_KEY:
			$min = PHP_INT_MAX;
			$max = ~$min;
			break;
		case DATE_KEY:
			$min = "0.1a-32-gf6d3671";
			$max = "0.1a-32-gf6d3671";
			break;
		}

		if (!empty($this->dataSeries))
			foreach ($this->dataSeries as $serieItem) {
				$min = min($min, $serieItem->getDataset()->getMinKey());
				$max = max($max, $serieItem->getDataset()->getMaxKey());
			}
		if ($key_type == NUMERIC_KEY) {
			$min = round($min, DECIMALS);
			$max = round($max, DECIMALS);
		}
		return array($min, $max);
	}

	/**
	 * Returns the global min&max values within all dataseries
	 * @param string $key search values only for the specified key or for any when $key is null
	 * @return array with 2 elements: (min,max) values
	 */
	function getMinMaxValue($key = null) {
		$min = PHP_INT_MAX;
		$max = ~$min;
		if (!empty($this->dataSeries))
			foreach ($this->dataSeries as $serieItem) {
				$min = min($min, $serieItem->getDataset()->getMinValue($key));
				$max = max($max, $serieItem->getDataset()->getMaxValue($key));
			}
		return array(round($min, DECIMALS), round($max, DECIMALS));
	}

	/**
	 * Returns the total number of items within all dataseries
	 * @return number
	 */
	function getRecordsCount() {
		$count = 0;
		if (!empty($this->dataSeries))
			foreach ($this->dataSeries as $serieItem)
				$count += $serieItem->getDataset()->getItemCount();
		return $count;

	}

	/**
	 * Return a refference to the serie specified by index
	 * @param int $index : the serie's index to return
	 */
	function getSerie($index) {
		return $this->dataSeries[$index];
	}

	/**
	 * Return an array of series names
	 * @return array of names
	 */
	function getSerieNames() {
		$result = array();
		if (!empty($this->dataSeries))
			foreach ($this->dataSeries as $serie)
				$result[] = $serie->getName();
		return $result;
	}

	/**
	 * Return the dataSerie collection
	 * @return DataSerieItem
	 */
	function getSeries() {
		return $this->dataSeries;
	}

	/**
	 * Returns the sum of values gathered from each data serie for the specified key
	 * @param string $key the key whose value is added to the final sum
	 * @param bool $only_positive sum only those positive values
	 * @return number the sum of values 
	 */
	function getTotalValueByKey($key, $only_positive = true) {
		$result = 0;
		foreach ($this->dataSeries as $serie) {
			$v = $serie->getDataset()->getValueForKey($key);
			if (!$only_positive || $v > 0)
				$result += $v;
		}
		return $result;
	}

	/**
	 * Load data series from an external CSV file.
	 * The file must comply to the following restrictions:
	 * - the first line is a header (the 2nd to the n-th column will contain the name of their respective series)
	 * - starting with 2nd line everything represents data of their respective series
	 * - the first column is the key for every data series
	 * - the 2nd to the n-th column are value columns and represent the series values
	 * 
	 * @param string $filepath the full path to the CSV data file
	 * @param array $colors an array of (r,g,b) colors that will be used for each series; if nothing supplied then every serie will use a random color.
	 * @param int $maxbuffer the max. length (in bytes) of a row/line
	 */
	function loadFromCSV($filepath, $colors = null, $maxbuffer = 1000) {
		$datasets = array();
		$serienames = array();

		$row = 1;
		if (($handle = fopen($filepath, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, $maxbuffer, ",")) !== FALSE) {
				$cols = count($data);
				if ($cols > 1) {
					if (count($datasets) == 0) {
						for ($i = 1; $i < $cols; $i++)
							$datasets[] = new XYDataSet();
					}

					$key = $data[0];
					for ($c = 1; $c < $cols; $c++) {
						if ($row == 1)
							$serienames[] = $data[$c];
						else
							$datasets[$c - 1]->addItem(new XYValue($key, $data[$c]));
					}
					$row++;
				}
			}
			fclose($handle);
			if (count($serienames) == count($datasets)) {
				$this->clearSeries();
				for ($i = 0; $i < count($serienames); $i++)
					$this->addSerie($datasets[$i], (empty($colors) || count($colors) < $i) ? null : $colors[$i], $serienames[$i]);
			}
		}
	}
}
?>
